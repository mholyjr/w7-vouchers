<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_w7vouchers
 *
 * @copyright   Copyright (C) 2018 - 2021 W7 Extensions. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die('Restricted access');

use Joomla\CMS\Router\Route;
use Joomla\CMS\Language\Text;

?>
<form id="login-form" action="<?php echo Route::_('index.php?option=com_w7vouchers&view=checkout&task=checkout.login', false); ?>" method="post">
    <div class="control-group">
        <div class="control-label">
            <label id="jform_first_name-lbl" for="login_email" class="required">
                <?php echo Text::_('COM_W7VOUCHERS_EMAIL'); ?><span class="star" aria-hidden="true">&nbsp;*</span>
            </label>
        </div>
        <div class="controls">
            <input 
                type="text" 
                name="email" 
                id="login_email" 
                value="" 
                class="form-control required" 
                required="true"
            >
        </div>
    </div>
    <div class="control-group">
        <div class="control-label">
            <label id="jform_first_name-lbl" for="login_password" class="required">
                <?php echo Text::_('COM_W7VOUCHERS_PASSWORD'); ?><span class="star" aria-hidden="true">&nbsp;*</span>
            </label>
        </div>
        <div class="controls">
            <input 
                type="password" 
                name="password" 
                id="login_password" 
                value="" 
                class="form-control required" 
                required="true"
            >
        </div>
    </div>
    <div class="mt-3">
        <button class="btn-primary btn"><?php echo Text::_('COM_W7VOUCHERS_LOGIN'); ?></button>
        <a class="forgot-password" href="<?php echo Route::_('index.php?option=com_users&view=reset', false); ?>"><?php echo Text::_('COM_W7VOUCHERS_FORGOT_PASSWORD'); ?></a>
    </div>
</form>