<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_w7vouchers
 *
 * @copyright   Copyright (C) 2018 - 2021 W7 Extensions. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die('Restricted access');

use Joomla\CMS\Language\Text;
use Joomla\CMS\Uri\Uri;

$src = $displayData['src'];
$alt = $displayData['alt'];
$class = $displayData['class'];

if(empty($src))
{
    $src = Uri::root() . '/components/com_w7vouchers/assets/images/empty.jpg';
}
else{
    $src = Uri::root() . $src;
}

?>

<img src="<?php echo $src; ?>" alt="<?php echo $alt; ?>" class="<?php echo $class; ?>">