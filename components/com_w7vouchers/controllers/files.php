<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_w7vouchers
 *
 * @copyright   Copyright (C) 2018 - 2021 W7 Extensions. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die('Restricted access');

use W7Extensions\Component\W7Vouchers\Site\Helpers\FilesHelper;
use Joomla\CMS\Factory;

/**
 * Files Controller
 *
 * @package     Joomla.Site
 * @subpackage  com_w7vouchers
 * @since       0.0.9
 */
class W7VouchersControllerFiles extends JControllerForm
{

    /**
     * Method to download a voucher
     * 
     * @return  void
     */
    public function download()
    {
        $app = Factory::getApplication();
        $input = $app->input;
        $filename = $input->get('filename', 'STRING', '');

        FilesHelper::downloadVoucher($filename, true);
    }

}