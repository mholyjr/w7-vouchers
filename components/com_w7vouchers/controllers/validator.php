<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_w7vouchers
 *
 * @copyright   Copyright (C) 2018 - 2021 W7 Extensions. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die('Restricted access');

use Joomla\CMS\Factory;
use Joomla\CMS\Response\JsonResponse;
use Joomla\Utilities\ArrayHelper;
use Joomla\CMS\Input\Input;
use Joomla\CMS\Router\Route;
use Joomla\CMS\Language\Text;

/**
 * Validator Controller
 *
 * @package     Joomla.Site
 * @subpackage  com_w7vouchers
 * @since       0.0.9
 */
class W7VouchersControllerValidator extends JControllerForm
{

    /**
	 * Proxy for getModel.
	 *
	 * @param   string  $name    The model name. Optional.
	 * @param   string  $prefix  The class prefix. Optional.
	 * @param   array   $config  Configuration array for model. Optional.
	 *
	 * @return  object  The model.
	 *
	 * @since   1.6
	 */
	public function getModel($name = 'Validator', $prefix = 'W7VouchersModel', $config = array('ignore_request' => true))
	{
		$model = parent::getModel($name, $prefix, $config);

		return $model;
    }

	public function logUsage()
	{
		$app = Factory::getApplication();
		$input = $app->input;
		$input = $app->input;
        $data = $input->post->getArray();
		$code = $data['jform']['code'];
		$model = $this->getModel();
		$url = $_SERVER['HTTP_REFERER'];
		
		if($model->logUsage($code))
		{
			$app->enqueueMessage(Text::_('COM_W7VOUCHERS_USAGE_LOGGED'), 'success');
			$this->setRedirect(Route::_($url, false));
		}
	}
}