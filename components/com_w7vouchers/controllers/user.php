<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_w7vouchers
 *
 * @copyright   Copyright (C) 2018 - 2021 W7 Extensions. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die('Restricted access');

use Joomla\CMS\Factory;
use Joomla\CMS\Response\JsonResponse;
use Joomla\Utilities\ArrayHelper;
use Joomla\CMS\Input\Input;
use Joomla\CMS\Router\Route;
use Joomla\CMS\User\UserHelper;
use Joomla\CMS\Language\Text;

/**
 * User Controller
 *
 * @package     Joomla.Site
 * @subpackage  com_w7vouchers
 * @since       0.0.9
 */
class W7VouchersControllerUser extends JControllerForm
{
    /**
     * Method to log in user
     * 
     * @return  void
     */
    public function login()
    {
        $db = Factory::getDbo();
        $app = Factory::getApplication();
        $input = $app->input;
        $data = $input->post->getArray();
        $url = $_SERVER['HTTP_REFERER'];

        $username = $data['email'];
        $password = $data['password'];

        if (empty($username) || empty($password))
        {
            $app->enqueueMessage(Text::_('COM_W7VOUCHERS_FILL_IN_FIELDS'), 'error');
            $this->setRedirect(Route::_($url, false)); 
            
            return false;
        }

        $query = $db->getQuery(true)
            ->select('id, password')
            ->from($db->quoteName('#__users'))
            ->where($db->quoteName('username') . ' = ' . $db->quote($username));

        $db->setQuery($query);

        $result = $db->loadObject();

        if (!$result)
        {
            $app->enqueueMessage(Text::_('COM_W7VOUCHERS_USER_DOES_NOT_EXIST'), 'error');
            $this->setRedirect(Route::_($url, false)); 
            
            return false;
        }

        if (UserHelper::verifyPassword($password, $result->password, $result->id))
        {
            $credentials = [
                'username' => $username,
                'password' => $password
            ];

            $response = $app->login($credentials);

            if (!$response)
            {
                throw new Exception($login_failed);
            }

            $app->enqueueMessage(Text::_('COM_W7VOUCHERS_LOG_IN_SUCCESS'), 'success');
			$this->setRedirect(Route::_($url, false));
        }
        else
        {
            $app->enqueueMessage(Text::_('COM_W7VOUCHERS_INVALID_PASSWORD'), 'error');
            $this->setRedirect(Route::_($url, false)); 
            
            return false;
        }
    }

    /**
     * Method to logout user
     * 
     * @return  void
     */
    public function logout()
    {
        $app = Factory::getApplication();
        $user = Factory::getUser();
        $url = $_SERVER['HTTP_REFERER'];

        $app->logout($user->id);

        $app->enqueueMessage(Text::_('COM_W7VOUCHERS_LOGOUT_SUCCESS'), 'success');
        $this->setRedirect(Route::_($url, false)); 
    }
}