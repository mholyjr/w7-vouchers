<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_w7vouchers
 *
 * @copyright   Copyright (C) 2018 - 2021 W7 Extensions. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die('Restricted access');

use Joomla\CMS\Factory;
use Joomla\CMS\Response\JsonResponse;
use Joomla\Utilities\ArrayHelper;
use Joomla\CMS\Input\Input;
use Joomla\CMS\Router\Route;
use Joomla\CMS\Language\Text;

/**
 * Cart Controller
 *
 * @package     Joomla.Site
 * @subpackage  com_w7vouchers
 * @since       0.0.9
 */
class W7VouchersControllerCart extends JControllerForm
{

    /**
	 * Proxy for getModel.
	 *
	 * @param   string  $name    The model name. Optional.
	 * @param   string  $prefix  The class prefix. Optional.
	 * @param   array   $config  Configuration array for model. Optional.
	 *
	 * @return  object  The model.
	 *
	 * @since   1.6
	 */
	public function getModel($name = 'Cart', $prefix = 'W7VouchersModel', $config = array('ignore_request' => true))
	{
		$model = parent::getModel($name, $prefix, $config);

		return $model;
    }

    /**
     * Method to add product to the cart
     * 
     * @return  void
     */
    public function addToCart(): void
    {
        $model = $this->getModel();
        $app = Factory::getApplication();
        $input = $app->input;
        $data = $input->post->getArray();
		$url = $_SERVER['HTTP_REFERER'];

        if($model->addToCart($data)) {
			$app->enqueueMessage(Text::_('COM_W7VOUCHERS_ADDED_TO_CART'), 'success');
			$this->setRedirect(Route::_($url, false));
		};
    }

	/**
	 * Method to add product to the cart from the list
	 * 
	 * @return	void
	 */
	public function addFromList(): void
	{
		$model = $this->getModel();
        $app = Factory::getApplication();
        $input = $app->input;
		$id = $input->get('id', 'INT', 0);

		$data = ['id_product' => $id, 'amount' => 1];

		$url = $_SERVER['HTTP_REFERER'];

        if($model->addToCart($data)) {
			$app->enqueueMessage(Text::_('COM_W7VOUCHERS_ADDED_TO_CART'), 'success');
			$this->setRedirect(Route::_($url, false));
		};
	}

	/**
	 * Method to get cart data
	 * 
	 * @return	void
	 */
	public function getCart(): void
	{
		$model = $this->getModel();
		$app = Factory::getApplication();
        $input = $app->input;
		$id_cart = $input->cookie->get('w7_vouchers_cart', 0);
		$data = $model->getCart($id_cart);

		echo new JsonResponse($data);
		$app->close();
	}

	/**
	 * Method to remove product from a cart
	 * 
	 * @return	void
	 */
	public function remove(): void
	{
		$model = $this->getModel();
		$app = Factory::getApplication();
        $input = $app->input;
        $data = $input->post->getArray();

		if($model->removeItem($data)) {
			echo new JsonResponse(JText::_('COM_W7VOUCHERS_REMOVED_FROM_CART', []));
			$app->close();
		};
	}
    
}