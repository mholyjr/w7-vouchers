<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_w7vouchers
 *
 * @copyright   Copyright (C) 2018 - 2021 W7 Extensions. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die('Restricted access');

use Joomla\CMS\Router\Route;
use Joomla\CMS\Language\Text;

$item = $this->item;

?>

<form id="validation_form" action="<?php echo Route::_('index.php?option=com_w7vouchers&task=validator.logUsage'); ?>" method="post">
    <?php if(!$this->item->active) : ?>
        <div class="w7-alert alert alert-danger" role="alert">
            <?php echo Text::_('COM_W7VOUCHERS_VOUCHER_NOT_ACTIVE'); ?>
        </div>
    <?php endif; ?>
    <?php 
    foreach($this->form->getFieldset('main') as $field) {
        echo $field->renderField();
    }
    ?>
    <?php if($this->item->active) : ?>
    <button class="btn-primary btn btn-fluid" <?php if($item->active != 1) : ?>disabled<?php endif; ?>>
        <?php echo Text::_('COM_W7VOUCHERS_LOG_USAGE'); ?>
    </button>
    <?php endif; ?>
</form>