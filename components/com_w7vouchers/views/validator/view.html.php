<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_w7vouchers
 *
 * @copyright   Copyright (C) 2018 - 2021 W7 Extensions. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access to this file
defined('_JEXEC') or die('Restricted access');

use Joomla\CMS\Factory;
use Joomla\CMS\Helper\ContentHelper;
use Joomla\CMS\Toolbar\ToolbarHelper;

/**
 * Validator View
 *
 * @since  0.0.1
 */
class W7VouchersViewValidator extends JViewLegacy
{
	/**
	 * Display the Validator view
	 *
	 * @param   string  $tpl  The name of the template file to parse; automatically searches through the template paths.
	 *
	 * @return  void
	 */
	public function display($tpl = null)
	{

        $app = Factory::getApplication();

		$this->item 	= $this->get('Item');
        $this->params   = $app->getParams();
		$this->form 	= $this->get('Form');

		parent::display($tpl);
	}
}