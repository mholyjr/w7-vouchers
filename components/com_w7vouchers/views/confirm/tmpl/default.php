<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_w7vouchers
 *
 * @copyright   Copyright (C) 2018 - 2021 W7 Extensions. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die('Restricted access');

use Joomla\CMS\Language\Text;

?>

<div class="w7vouchers" id="confirm">
    <div class="content">
        <img src="components/com_w7vouchers/assets/images/confirm.svg" alt="Confirm order illustration" class="confirm_illustration">
        <h1><?php echo Text::_('COM_W7VOUCHERS_ORDER_SUCCESSFUL'); ?></h1>
    </div>
</div>