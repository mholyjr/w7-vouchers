<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_w7vouchers
 *
 * @copyright   Copyright (C) 2018 - 2021 W7 Extensions. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die('Restricted access');

use Joomla\CMS\HTML\HTMLHelper;
use Joomla\CMS\Language\Text;
use W7Extensions\Component\W7Vouchers\Site\Helpers\DisplayHelper;
use Joomla\CMS\Layout\FileLayout;
use Joomla\Registry\Registry;
use Joomla\CMS\Router\Route;

$item = $this->item;

?>

<div id="item-<?php echo $item->id; ?>" class="grid_item card voucher_active-<?php echo $this->escape($item->active); ?>">
    <div class="card-body">
        <div class="item_title_wrapper">
            <h2 class="card-title"><?php echo $this->escape($item->title); ?></h2>
        </div>
        <div class="item_info_wrapper">
            <div class="row mb-2 pb-2 border-bottom">
                <div class="col">
                    <span class='table-item-label'><?php echo Text::_('COM_W7VOUCHERS_CODE'); ?></span>
                </div>
                <div class="col">
                    <span class='table-item-value'><?php echo $this->escape($item->code); ?></span>
                </div>
            </div>
            <div class="row mb-2 pb-2 border-bottom">
                <div class="col">
                    <span class='table-item-label'><?php echo Text::_('COM_W7VOUCHERS_VALID_UNTIL'); ?></span>
                </div>
                <div class="col">
                    <span class='table-item-value'><?php echo HtmlHelper::date($item->expires, Text::_('DATE_FORMAT_LC5')); ?></span>
                </div>
            </div>
            <div class="row mb-2 pb-2 border-bottom">
                <div class="col">
                    <span class='table-item-label'><?php echo Text::_('COM_W7VOUCHERS_USED'); ?></span>
                </div>
                <div class="col">
                    <span class='table-item-value'><?php echo $this->escape($item->used); ?>/<?php echo $this->escape($item->max_usage); ?></span>
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <a 
                        href="<?php echo $item->active ? Route::_('index.php?option=com_w7vouchers&task=files.download&filename=' . $item->filename, false) : '#'; ?>" 
                        target="blank" class="btn-primary btn"
                        <?php echo !$item->active ? 'disabled' : ''; ?>
                    >
                        <?php echo Text::_('COM_W7VOUCHERS_DOWNLOAD'); ?>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>