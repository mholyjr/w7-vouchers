<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_w7vouchers
 *
 * @copyright   Copyright (C) 2018 - 2021 W7 Extensions. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die('Restricted access');

use Joomla\CMS\Language\Text;
use Joomla\CMS\Layout\FileLayout;

$items = (array)$this->items;
$params = $this->params;
$layout = $this->params->get('layout', 'grid');
$n = count($items);
$columns = $this->params->get('cols', 3);
$rows = ceil($n/$columns);
$class = 'col-sm-' . 12/$columns;
$login_form_layout = new FileLayout('loginForm', JPATH_ROOT .'/components/com_w7vouchers/layouts');
$login_form = $login_form_layout->render([]);

?>
<div class="w7vouchers">
    <?php if($this->user->id !== 0 && count($items)) : ?>
    <div id="items_wrapper">
        <h1 class="page_title">
            <?php echo Text::_('COM_W7VOUCHERS_MY_VOUCHERS'); ?>
        </h1>
        <div class="row">
            <?php 
                if($layout == 'grid'):
                for ($i = 0, $n; $i < $n; $i ++) :
                $item = $items[$i];
                
                if($rows > 1 && $i !== 0 && $i % $columns == 0) {
                    echo '</div><div class="row">';
                }
            ?>
            <div class="<?php echo $class; ?>">
                <?php
                    $this->item = &$item;
                    echo $this->loadTemplate('grid'); 
                ?>
            </div>
            <?php 
                endfor; 
                else:
            ?>
            <div></div>
            <?php endif; ?>
            <?php echo $this->pagination->getListFooter(); ?>
        </div>
    </div>
    <?php elseif($this->user->id !== 0 && !count($items)) : ?>
        <div id="empty">
            <div class="content">
                <img src="components/com_w7vouchers/assets/images/no-data.svg" alt="No data illustration" class="no_data_illustration">
                <h1><?php echo Text::_('COM_W7VOUCHERS_NO_VOUCHERS'); ?></h1>
            </div>
        </div>
    <?php else : ?>
    <div id="needs_login">
        <div id="login_wrapper">
            <h1 class="page_title text-center"><?php echo Text::_('COM_W7VOUCHERS_NEEDS_LOGIN'); ?></h1>
            <?php echo $login_form; ?>
        </div>
    </div>
    <?php endif; ?>
</div>