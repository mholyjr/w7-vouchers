<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_w7vouchers
 *
 * @copyright   Copyright (C) 2018 - 2021 W7 Extensions. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die('Restricted access');

use Joomla\CMS\Factory;
use Joomla\CMS\Helper\ContentHelper;
use Joomla\CMS\Toolbar\ToolbarHelper;

/**
 * User's vouchers View
 *
 * @since  0.0.1
 */
class W7VouchersViewUsersVouchers extends JViewLegacy
{
	/**
	 * Display the Profile view
	 *
	 * @param   string  $tpl  The name of the template file to parse; automatically searches through the template paths.
	 *
	 * @return  void
	 */
	public function display($tpl = null)
	{
        $app = Factory::getApplication();

		$this->user 		= Factory::getUser();
        $this->params 		= $app->getParams();

		if($this->user->id != 0)
		{
			$this->items 		= $this->get('Items');
			$this->pagination 	= $this->get('Pagination');
		}
		else
		{
			$this->items = array();
		}

		parent::display($tpl);
	}

}