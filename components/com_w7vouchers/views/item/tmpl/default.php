<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_w7vouchers
 *
 * @copyright   Copyright (C) 2018 - 2021 W7 Extensions. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die('Restricted access');

use W7Extensions\Component\W7Vouchers\Site\Helpers\DisplayHelper;
use Joomla\CMS\Language\Text;
use Joomla\CMS\Layout\FileLayout;
use Joomla\CMS\Router\Route;

$item = $this->item;

$image_data = [
    'src' => !empty($item->images) ? $item->images['main_image'] : '', 
    'alt' => !empty($item->images) ? $item->images['main_image_alt'] : '', 
    'class' => 'w7_image img-fluid'
];
$image_layout = new JLayoutFile('image', JPATH_SITE .'/components/com_w7vouchers/layouts');
$main_image = $image_layout->render($image_data);

$show_stores = $this->item->params->get('show_stores', 1);
$stores_layout = $this->item->params->get('stores_layout', 'badge');
$badge_label = $this->item->params->get('badge_label', 'title');
$badge_link = $this->item->params->get('badge_link', 0);
$display_image = $this->item->params->get('display_image', 1);
$display_title = $this->item->params->get('display_title', 1);
$title_tag = $this->item->params->get('title_tag', 'h1');
$display_intro = $this->item->params->get('display_intro', 1);
$display_desc = $this->item->params->get('display_desc', 1);

?>
<div id="voucher_item" class="w7vouchers">
    <div class="row">
        <div class="col-sm-8">
            <?php if($display_image) : ?>
            <div class="image_wrapper mb-4">
                <?php echo $main_image; ?>
            </div>
            <?php endif; ?>
            <div class="info_wrapper">

                <?php if($display_title) : ?>
                <<?php echo $title_tag; ?> class="item_title">
                    <?php echo $this->escape($item->title); ?>
                </<?php echo $title_tag; ?>>
                <?php endif; ?>

                <?php if($display_intro) : ?>
                <div class="description_short">
                    <?php echo $item->description_short; ?>
                </div>
                <?php endif; ?>

                <?php if($display_desc) : ?>
                <div class="description_full">
                    <?php echo $item->description_full; ?>
                </div>
                <?php endif; ?>

                <?php if($show_stores) : ?>
                <div id="stores_list">
                    <h2 class="info_heading"><?php echo Text::_('COM_W7VOUCHERS_CAN_BE_REDEEMED_AT'); ?></h2>
                    <?php foreach($item->stores as $store) : ?>
                        <?php if($stores_layout == 'badge') : ?>
                        <?php if($badge_link) : ?>
                        <a href="<?php echo Route::_('index.php?option=com_w7vouchers&view=store&id=' . $store->id, false); ?>">
                        <?php endif; ?>
                        <span class="badge bg-secondary">
                            <?php 
                                switch($badge_label)
                                {
                                    case 'title':
                                        echo $this->escape($store->title); 
                                    break;
                                    case 'address':
                                        echo $this->escape($store->address); 
                                    break;
                                }
                            ?>
                        </span>
                        <?php if($badge_link) : ?>
                        </a>
                        <?php endif; ?>
                        <?php endif; ?>
                    <?php endforeach; ?>
                </div>
                <?php endif; ?>
            </div>
        </div>
        <div class="col-sm-4">
            <form method="post" action="<?php echo Route::_('index.php?option=com_w7vouchers&task=cart.addToCart'); ?>">
                <div class="actions">
                    <div class="price_wrapper">
                        <span class="price">
                        <?php if(empty($item->special_price)) : ?>
                            <?php echo $this->escape(DisplayHelper::displayPrice($item->price)); ?>
                        <?php else : ?>
                            <span class="regualar_price">
                                <?php echo $this->escape(DisplayHelper::displayPrice($item->price)); ?>
                            </span>
                            <?php echo $this->escape(DisplayHelper::displayPrice($item->special_price)); ?>
                        <?php endif; ?>
                        </span>
                        <?php if($item->price_note) : ?>
                        <span class="price_note small">
                            <?php echo $this->escape($item->price_note); ?>
                        </span>
                        <?php endif; ?>
                    </div>
                    <div class="buttons_wrapper">
                        <div class="row">
                            <div class="col-sm-3 pe-0">
                                <input type="number" id="amount" name="amount" class="form-control" value="1" step="1" min="1">
                            </div>
                            <div class="col-sm-9">
                                <button class="btn-primary btn"><?php echo Text::_('COM_W7VOUCHERS_ADD_TO_CART'); ?></button>
                            </div>
                        </div>
                        <input name="id_product" type="hidden" value="<?php echo $item->id; ?>">
                        <?php echo JHtml::_('form.token'); ?>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>