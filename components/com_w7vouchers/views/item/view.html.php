<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_w7vouchers
 *
 * @copyright   Copyright (C) 2018 - 2021 W7 Extensions. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access to this file
defined('_JEXEC') or die('Restricted access');

use Joomla\CMS\Factory;
use Joomla\CMS\Helper\ContentHelper;
use Joomla\CMS\Toolbar\ToolbarHelper;
use W7Extensions\Component\W7Vouchers\Site\Helpers\DisplayHelper;

/**
 * Item View
 *
 * @since  0.0.1
 */
class W7VouchersViewItem extends JViewLegacy
{
	/**
	 * Display the Item view
	 *
	 * @param   string  $tpl  The name of the template file to parse; automatically searches through the template paths.
	 *
	 * @return  void
	 */
	public function display($tpl = null)
	{

        $app = Factory::getApplication();

		$this->item 		= $this->get('Item');
        $this->params   	= $app->getParams();

		parent::display($tpl);

		$this->setDocument();
	}

	/**
	 * Method to set up the document properties
	 *
	 * @return void
	 */
	protected function setDocument() 
	{

		$item = $this->item;
		$params = $this->item->params;

		$data_tags = [
			'[ITEM_TITLE]' => $item->title,
			'[CATEGORY_TITLE]' => $item->cat_title,
			'[INTRO_TEXT]' => strip_tags($item->description_short),
			'[PRICE]' => DisplayHelper::displayPrice($item->price)
		];

		$title = strtr($params->get('page_title'), $data_tags);

		$document = Factory::getDocument();
		$document->setTitle($title);
	}
}