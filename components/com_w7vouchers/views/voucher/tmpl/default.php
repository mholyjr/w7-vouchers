<?php 
/**
 * @package     Joomla.Template
 * @subpackage  W7 Vouchers
 *
 * @copyright   Copyright (C) 2005 - 2018 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die('Restricted access');

use Joomla\CMS\Factory;
use W7Extensions\Component\W7Vouchers\Site\Helpers\DisplayHelper;
use Joomla\CMS\HTML\HTMLHelper;
use Joomla\CMS\Language\Text;

$app = Factory::getApplication();
$code = $app->input->get('code');
$price =  $app->input->get('price');
$price = DisplayHelper::displayPrice($price);
$expires = $app->input->get('expires');

?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8" />
    <style>
      @import url("https://fonts.googleapis.com/css2?family=Glass+Antiqua&family=La+Belle+Aurore&display=swap");
      body {
        margin: 0;
        padding: 5px 0 0 5px;
      }
      #container {
        background-image: url("templates/w7vouchers/bg.jpg");
        width: 576px;
        height: 216px;
        display: flex;
        align-items: center;
        justify-content: center;
      }
      #content {
        width: 100%;
        padding: 30px;
      }
      h1 {
        text-align: center;
        font-size: 32px;
      }
      h1,
      h2,
      h3 {
        line-height: 1.5;
        margin: 0;
        font-weight: 400;
        font-family: "Glass Antiqua", cursive;
      }
      h3 {
        font-size:18px;
      }
      p {
        font-family: "La Belle Aurore", cursive;
        font-size: 22px;
        margin: 0;
        line-height: 1;
        font-weight: 400;
        text-align: center;
      }
      .row {
        width: 100%;
        display: flex;
        flex-wrap: wrap;
        justify-content: space-between;
      }
      .col {
        width: 50%;
      }
      .col.left h1 {
        font-size: 1.5em;
        text-align: left;
      }
      .col.right h2,
      .col.right h3 {
        text-align: right;
      }
    </style>
  </head>
  <body>
    <div id="container">
      <div id="content">
        <h1 style="line-height: 1;">Dárkový poukaz</h1>
        <h1>BUTTERFLY</h1>
        <p>Masážní a regenerační salon</p>
        <div class="row">
          <div class="col left">
            <h1><?php echo $price; ?></h1>
            <h3>Číslo poukazu: <?php echo $code; ?><br>Platnost: <?php echo HtmlHelper::date($expires, Text::_('DATE_FORMAT_LC3')); ?></h3>
          </div>
          <div class="col right">
            <h2>U Náspu 582 Liberc 1</h2>
            <h3>Email: kocovam@volny.cz<br>Tel.: 605110154</h3>
          </div>
        </div>
      </div>
    </div>
  </body>
</html>
