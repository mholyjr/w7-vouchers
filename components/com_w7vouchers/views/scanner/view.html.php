<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_w7vouchers
 *
 * @copyright   Copyright (C) 2018 - 2021 W7 Extensions. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die('Restricted access');

use Joomla\CMS\Factory;
use Joomla\CMS\Helper\ContentHelper;
use Joomla\CMS\Toolbar\ToolbarHelper;

/**
 * Scanner View
 *
 * @since  0.0.1
 */
class W7VouchersViewScanner extends JViewLegacy
{
	/**
	 * Display the Scanner view
	 *
	 * @param   string  $tpl  The name of the template file to parse; automatically searches through the template paths.
	 *
	 * @return  void
	 */
	public function display($tpl = null)
	{

        $app = Factory::getApplication();
		$document = Factory::getDocument();
        $document->addScript('components/com_w7vouchers/assets/js/scanner.js');
		
        $this->params = $app->getParams();

		parent::display($tpl);
	}
}