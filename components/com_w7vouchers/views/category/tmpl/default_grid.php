<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_w7vouchers
 *
 * @copyright   Copyright (C) 2018 - 2021 W7 Extensions. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die('Restricted access');

use Joomla\CMS\HTML\HTMLHelper;
use Joomla\CMS\Language\Text;
use W7Extensions\Component\W7Vouchers\Site\Helpers\DisplayHelper;
use Joomla\CMS\Layout\FileLayout;
use Joomla\Registry\Registry;
use Joomla\CMS\Router\Route;

$item = $this->item;

$item_link = JRoute::_('index.php?option=com_w7vouchers&catid='.$item->catslug.'&view=item&id=' . $item->id);

$item->imgs = new Registry;
$item->imgs->loadString($item->images);

$image_data = ['src' => $item->imgs->get('intro_image'), 'alt' => $item->imgs->get('intro_image_alt'), 'class' => 'card-img-top'];
$image_layout = new JLayoutFile('image', JPATH_SITE .'/components/com_w7vouchers/layouts');
$intro_image = $image_layout->render($image_data);

?>
<div id="item-<?php echo $item->id; ?>" class="grid_item card">
    <?php echo $intro_image; ?>
    <div class="card-body">
        <div class="item_title_wrapper">
            <a href="<?php echo $item_link; ?>"><h2 class="card-title"><?php echo $this->escape($item->title); ?></h2></a>
        </div>
        <div class="item_description_wrapper">
            <?php echo $item->description_short; ?>
        </div>
        <div class="item_price_wrapper">
            <span class="item_price">
            <?php if(empty($item->special_price)) : ?>
                <?php echo $this->escape(DisplayHelper::displayPrice($item->price)); ?>
                <?php else : ?>
                <span class="regualar_price">
                    <?php echo $this->escape(DisplayHelper::displayPrice($item->price)); ?>
                </span>
                    <?php echo $this->escape(DisplayHelper::displayPrice($item->special_price)); ?>
                <?php endif; ?>
            </span>
        </div>
        <a href="<?php echo $item_link; ?>" class="card-link btn"><?php echo Text::_('COM_W7VOUCHERS_SEE_DETAIL'); ?></a>
        <a href="<?php echo Route::_('index.php?option=com_w7vouchers&task=cart.addFromList&id=' . $item->id, false); ?>" class="card-link btn btn-primary"><?php echo Text::_('COM_W7VOUCHERS_BUY_VOUCHER'); ?></a>
    </div>
</div>