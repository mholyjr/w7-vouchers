<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_w7vouchers
 *
 * @copyright   Copyright (C) 2018 - 2021 W7 Extensions. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die('Restricted access');

use Joomla\CMS\HTML\HTMLHelper;
use Joomla\CMS\Language\Text;

$items = $this->items;
$params = $this->params;
$layout = $this->params->get('layout', 'grid');
$n = count($items);
$columns = $this->params->get('cols', 2);
$rows = ceil($n/$columns);
$class = 'col-sm-' . 12/$columns;

?>
<div class="w7vouchers" id="category_view">
    <div id="category_info">
        <?php if ($this->params->get('show_page_heading')) : ?>
            <div class="page-header">
                <h1><?php echo $this->escape($this->params->get('page_heading')); ?></h1>
            </div>
        <?php endif; ?>
    </div>
    <div id="items_wrapper">
        <div class="row">
            <?php 
                if($layout == 'grid'):
                for ($i = 0, $n; $i < $n; $i ++) :
                $item = $items[$i];
                
                if($rows > 1 && $i !== 0 && $i % $columns == 0) {
                    echo '</div><div class="row mb-3">';
                }
            ?>
            <div class="<?php echo $class; ?>">
                <?php
                    $this->item = &$item;
                    echo $this->loadTemplate('grid'); 
                ?>
            </div>
            <?php 
                endfor; 
                else:
            ?>
            <div></div>
            <?php endif; ?>
            <?php echo $this->pagination->getListFooter(); ?>
        </div>
    </div>
</div>