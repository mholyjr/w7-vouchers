<?php

/**
 * @package     Joomla.Site
 * @subpackage  com_w7vouchers
 *
 * @copyright   Copyright (C) 2018 - 2021 W7 Extensions. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access to this file
defined('_JEXEC') or die('Restricted access');

use Joomla\CMS\Factory;
use Joomla\CMS\Helper\ContentHelper;
use Joomla\CMS\Toolbar\ToolbarHelper;

/**
 * Category View
 *
 * @since  0.0.1
 */
class W7VouchersViewCategory extends JViewLegacy
{
	/**
	 * Display the Category view
	 *
	 * @param   string  $tpl  The name of the template file to parse; automatically searches through the template paths.
	 *
	 * @return  void
	 */
	public function display($tpl = null)
	{

		$app = Factory::getApplication();

		$this->id       	= $this->get('Id');
		$this->items 		= $this->get('Items');
		$this->params   	= $app->getParams();
		$this->pagination 	= $this->get('Pagination');
		$this->catTitle 	= $this->get('Title');

		parent::display($tpl);

		$this->setDocument();
	}

	/**
	 * Method to set up the document properties
	 *
	 * @return void
	 */
	protected function setDocument()
	{
		$page_title = $this->params->get('page_title');
		if (empty($page_title)) {
			$page_title = $this->catTitle;
		}

		$document = Factory::getDocument();
		$document->setTitle($page_title);
	}
}
