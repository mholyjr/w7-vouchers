<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_w7vouchers
 *
 * @copyright   Copyright (C) 2018 - 2021 W7 Extensions. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access to this file
defined('_JEXEC') or die('Restricted access');

use Joomla\CMS\Factory;
use Joomla\CMS\Helper\ContentHelper;
use Joomla\CMS\Toolbar\ToolbarHelper;
use W7Extensions\Component\W7Vouchers\Site\Helpers\CartHelper;
use W7Extensions\Component\W7Vouchers\Site\Helpers\PdfHelper;

/**
 * Checkout View
 *
 * @since  0.0.1
 */
class W7VouchersViewCheckout extends JViewLegacy
{
	/**
	 * Display the Checkout view
	 *
	 * @param   string  $tpl  The name of the template file to parse; automatically searches through the template paths.
	 *
	 * @return  void
	 */
	public function display($tpl = null)
	{

        $app = Factory::getApplication();

		$this->form         = $this->get('Form');
        $this->params   	= $app->getParams();
		$this->id_cart		= CartHelper::getCartCookie();
		$this->products		= CartHelper::getProducts((int)$this->id_cart);
		$this->user 	 	= Factory::getUser();

		parent::display($tpl);
	}
}