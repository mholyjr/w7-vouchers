<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_w7vouchers
 *
 * @copyright   Copyright (C) 2018 - 2021 W7 Extensions. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die('Restricted access');

use Joomla\CMS\HTML\HTMLHelper;
use Joomla\CMS\Language\Text;
use Joomla\CMS\Router\Route;
use Joomla\CMS\Layout\FileLayout;

$user = $this->user;
$id_user = $user->id;
$login_form_layout = new FileLayout('loginForm', JPATH_ROOT .'/components/com_w7vouchers/layouts');
$login_form = $login_form_layout->render([]);

?>
<?php if(empty($id_user)) :?>
    <h2><?php echo Text::_('COM_W7VOUCHERS_ALREADY_HAVE_ACCOUNT'); ?></h2>
    <?php echo $login_form; ?>
<?php else : ?> 
<div id="signed_in_as">
    <div>
        <span><?php echo Text::_('COM_W7VOUCHERS_SIGNED_IN_AS'); ?><span class="bold"> <?php echo $user->email; ?></span></span>
    </div>
    <div>
        <a href="<?php echo Route::_('index.php?option=com_w7vouchers&task=user.logout'); ?>" class="btn btn-danger"><?php echo Text::_('COM_W7VOUCHERS_LOG_OUT'); ?></a>
    </div>
</div>
<?php endif; ?>