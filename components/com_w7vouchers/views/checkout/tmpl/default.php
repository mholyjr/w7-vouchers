<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_w7vouchers
 *
 * @copyright   Copyright (C) 2018 - 2021 W7 Extensions. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die('Restricted access');

use Joomla\CMS\HTML\HTMLHelper;
use Joomla\CMS\Language\Text;

?>

<div class="w7vouchers">
    <div class="row">
        <div class="col-sm-6 pe-5">
            <div id="login_form_wrapper">
                <?php echo $this->loadTemplate('login'); ?>
            </div>
            <form id="order-form" action="<?php echo JRoute::_('index.php?option=com_w7vouchers&view=checkout&task=checkout.checkout', false); ?>" method="post">
                <?php 
                foreach($this->form->getFieldset('main') as $field) {
                    echo $field->renderField();
                }
                foreach($this->form->getFieldset('custom') as $field) {
                    echo $field->renderField();
                }
                ?>
                <button class="btn-primary btn"><?php echo Text::_('COM_W7VOUCHERS_FINISH_ORDER'); ?></button>
            </form>
        </div>
        <div class="col-sm-6">
            <h4><?php echo Text::_('COM_W7VOUCHERS_PRODUCTS_IN_CART'); ?></h4>
            <?php echo $this->loadTemplate('products'); ?>
        </div>
    </div>
</div>


