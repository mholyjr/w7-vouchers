<?php

/**
 * @package     Joomla.Site
 * @subpackage  com_w7vouchers
 *
 * @copyright   Copyright (C) 2018 - 2021 W7 Extensions. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die('Restricted access');

use W7Extensions\Component\W7Vouchers\Site\Helpers\DisplayHelper;

?>

<div id="products-block">
    <?php
    foreach ($this->products as $product) :
        $image_data = [
            'src' => !empty($product->images) ? $product->images->intro_image : '',
            'alt' => !empty($product->images) ? $product->images->intro_image_alt : '',
            'class' => 'w7_image img-fluid'
        ];
        $image_layout = new JLayoutFile('image', JPATH_SITE . '/components/com_w7vouchers/layouts');
        $intro_image = $image_layout->render($image_data);

        $total_price = $product->amount * $product->price;

    ?>
        <div class="row cart-product-item">
            <div class="col-sm-3">
                <?php echo $intro_image; ?>
            </div>
            <div class="col-sm-8 pe-0">
                <div class="row">
                    <div class="col-sm-12">
                        <span class="product_title"><?php echo $product->title; ?></span>
                    </div>
                
                    <div class="row mb-1">
                        <div class="col-sm-12">
                            <span><?php echo DisplayHelper::displayShortDesc($product->description_short, 40); ?></span>
                        </div>
                    </div>
                    <div class="row align-items-center">
                        <div class="col-sm-12 pe-0 justify-content-start align-items-center d-flex">
                            <span class="cart-amount me-3">
                                <?php echo $this->escape($product->amount); ?>
                            </span>
                            <span class="cart-price">
                                <?php echo DisplayHelper::displayPrice($total_price); ?>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <?php endforeach; ?>
</div>