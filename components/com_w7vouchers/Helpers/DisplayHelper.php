<?php

namespace W7Extensions\Component\W7Vouchers\Site\Helpers;

use Joomla\CMS\Component\ComponentHelper;
use Joomla\CMS\Language\Text;

/**
 * @package     Joomla.Site
 * @subpackage  com_w7vouchers
 *
 * @copyright   Copyright (C) 2018 - 2021 W7 Extensions. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

class DisplayHelper
{

    /**
     * Method to display formatted price
     * 
     * @param   int     $price
     * 
     * @return  string
     */
    public static function displayPrice(int $price): string
    {

        if($price != 0)
        {
            $params                 = ComponentHelper::getParams('com_w7vouchers');
            $currency_symbol        = $params->get('currency_symbol', '€');
            $currency_symbol_pos    = $params->get('currency_symbol_pos', 'left');
            $num_of_decimals        = $params->get('decimals', 2);
            $decimals_sep           = $params->get('decimals_sep', ',');
            $thousands_sep          = $params->get('thousands_sep', ' ');
            $price_format           = '';
    
            $price = number_format($price, $num_of_decimals, $decimals_sep, $thousands_sep);
    
            if($currency_symbol_pos == 'left')
            {
                $price_format = $currency_symbol . $price;
            }
            else 
            {
                $price_format = $price . $currency_symbol;
            }
        }
        else 
        {
            $price_format = Text::_('COM_W7VOUCHERS_FREE');
        }
        

        return $price_format;
    }

    /**
     * Method to display short description
     * 
     * @param   string  $str
     * @param   int     $length
     * 
     * @return  string
     */
    public static function displayShortDesc(string $str, int $length = 120): string
    {
        return mb_strimwidth($str, 0, $length, "...");
    }

    public static function trueFalse(int $val): string
    {
        if($val)
        {
            return 'true';
        }

        return 'false';
    }

}