<?php

namespace W7Extensions\Component\W7Vouchers\Site\Helpers;

/**
 * @package     Joomla.Site
 * @subpackage  com_w7vouchers
 *
 * @copyright   Copyright (C) 2018 - 2021 W7 Extensions. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

use Joomla\CMS\Filesystem\Folder;
use Joomla\CMS\Uri\Uri;
use Joomla\CMS\Component\ComponentHelper;
use W7Extensions\Component\W7Vouchers\Site\Helpers\DisplayHelper;

class PdfHelper
{

    private static $mediaPath = JPATH_SITE . '/media/com_w7vouchers/pdf';

    /**
     * Method to create PDF file
     * and save it to media folder
     * 
     * @param   object  $voucher
     * 
     * @return  void
     */
    public static function createPdf(object $voucher)
    {
        $params = ComponentHelper::getParams('com_w7vouchers');
        $api_key = $params->get('browserless');
        $options = [
            'url' => Uri::root() . "index.php?option=com_w7vouchers&view=voucher&code=$voucher->code&price=$voucher->price&expires=$voucher->expires",
            //'html' => '<p>'.json_encode($voucher).'</p>',
            "gotoOptions" => [
                "waitUntil" => "networkidle2",
            ],
            'options' => [
                'displayHeaderFooter' => false,
                'printBackground' => true,
                'landscape' => false,
                'width' => '596px',
                'height' => '236px',
                'pageRanges' => '1'
            ]
        ];

        $curl = curl_init();

        curl_setopt_array($curl, [
            CURLOPT_URL => "https://chrome.browserless.io/pdf?token=" . $api_key,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => json_encode($options),
            CURLOPT_HTTPHEADER => [
                "Content-Type: application/json"
            ],
        ]);

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            echo "cURL Error #:" . $err;
        } else {
            if (!Folder::exists(self::$mediaPath . '/' . $voucher->id_order)) {
                Folder::create(self::$mediaPath . '/' . $voucher->id_order);
            }
            file_put_contents(self::$mediaPath . '/' . $voucher->id_order . '/' . $voucher->filename . '.pdf', $response);
        }
    }
}
