<?php

namespace W7Extensions\Component\W7Vouchers\Site\Helpers;

/**
 * @package     Joomla.Site
 * @subpackage  com_w7vouchers
 *
 * @copyright   Copyright (C) 2018 - 2021 W7 Extensions. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

 defined('_JEXEC') or die('Restricted access');

use Joomla\CMS\Factory;

class PaymentHelper 
{
    public function checkStripe($stripe, $order, $cookie = true, $redirect = true, $exit = true)
    {
        $params = json_decode($order->params);
        $ua = array('bindings_version' => '7.17.0', 'lang' => 'php',
            'lang_version' => phpversion(), 'publisher' => 'stripe', 'uname' => php_uname());
        $headers = array('X-Stripe-Client-User-Agent: '.json_encode($ua),
            'User-Agent: Stripe/v1 PhpBindings/7.17.0',
            'Authorization: Bearer '.$stripe->params->secret_key);
        $url = 'https://api.stripe.com/v1/checkout/sessions/'.$params->id;
        $curl = curl_init();
        $options = [];
        $options[CURLOPT_HTTPGET] = 1;
        $options[CURLOPT_URL] = $url;
        $options[CURLOPT_CONNECTTIMEOUT] = 30;
        $options[CURLOPT_TIMEOUT] = 80;
        $options[CURLOPT_RETURNTRANSFER] = true;
        $options[CURLOPT_HTTPHEADER] = $headers;
        $options[CURLOPT_SSL_VERIFYPEER] = false;
        curl_setopt_array($curl, $options);
        $body = curl_exec($curl);
        curl_close($curl);
        $json = json_decode($body);
        if ($json->status == 'complete') {
            $this->approveOrder($order->id, $body, true, $cookie, $redirect, $exit);
        } else if ($json->status == 'expired') {
            $this->setCanceled($order);
            $this->approveOrder(0, null, false, $cookie, $redirect, $exit);
        } else {
            $this->approveOrder(0, null, false, $cookie, $redirect, $exit);
        }
    }
}