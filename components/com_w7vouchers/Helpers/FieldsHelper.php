<?php

namespace W7Extensions\Component\W7Vouchers\Site\Helpers;

use Joomla\CMS\Factory;
use Joomla\CMS\Component\ComponentHelper;
use Joomla\CMS\Language\Text;

/**
 * @package     Joomla.Site
 * @subpackage  com_w7vouchers
 *
 * @copyright   Copyright (C) 2018 - 2021 W7 Extensions. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

class FieldsHelper
{

    /**
	 * Method to get form fields
	 * 
	 * @return	array
	 */
	public static function getFormFields()
	{
		$db = Factory::getDbo();
		$query = $db->getQuery(true);

		$query->select('*')
			->from($db->quoteName('#__w7vouchers_fields'))
			->where($db->quoteName('published') . ' = 1')
			->where($db->quoteName('core') . ' = 0')
			->order($db->escape('ordering') . ' ASC');

		$db->setQuery($query);

		$fields = $db->loadObjectList();

		return $fields;
	}

}