<?php

namespace W7Extensions\Component\W7Vouchers\Site\Helpers;

use Joomla\CMS\Factory;
use Joomla\CMS\Component\ComponentHelper;

/**
 * @package     Joomla.Site
 * @subpackage  com_w7vouchers
 *
 * @copyright   Copyright (C) 2018 - 2021 W7 Extensions. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

class EmailsHelper
{
    /**
     * Method to get html body of an email
     * 
     * @param   string  $type
     * 
     * @return  object
     */
    public static function getEmailContent(string $type): object
    {
        $db = Factory::getDbo();
        $query = $db->getQuery(true);

        $query->select('content, subject, params, usertype')
            ->from($db->quoteName('#__w7vouchers_emails'))
            ->where($db->quoteName('type') . ' = :type')
            ->bind(':type', $type);

        $db->setQuery($query);
        $data = $db->loadObject();

        return $data;
    }

    /**
     * Method to send email
     * 
     * @param   object  $params
     * @param   string  $type
     * @param   array   $attachments
     * 
     * @return  bool
     */
    public static function sendEmail(
        object $params, 
        string $type, 
        array $attachments = array()
    ): bool
    {

        $config_params = ComponentHelper::getParams('com_w7vouchers');

        $email_content = self::getEmailContent($type);
        $email_params = json_decode($email_content->params);

        if($email_content->usertype == 'admin' && $email_params->send == 0)
        {
            return false;
        }

        $data_tags = array(
            "[FIRST_NAME]" => $params->first_name,
            "[LAST_NAME]" => $params->last_name,
            "[COMPANY_NAME]" => $config_params->get('company_name'),
            "[COMPANY_ADDRESS]" => $config_params->get('company_address'),
            "[COMPANY_POSTCODE]" => $config_params->get('company_postcode'),
            "[COMPANY_CITY]" => $config_params->get('company_city'),
            "[COMPANY_VAT_NUMBER]" => $config_params->get('company_vat_number'),
            "[COMPANY_REG_NUMBER]" => $config_params->get('company_reg_number')
        );

        $body = strtr($email_content->content, $data_tags);

        $mailer = Factory::getMailer();
        $config = Factory::getConfig();

        $sender = array( 
            $config->get('mailfrom'),
            $config->get('fromname') 
        );
        
        $mailer->setSubject($email_content->subject);
        $mailer->setSender($sender);

        if($email_content->usertype == 'admin' && $email_params->send == 1)
        {
            $mailer->addRecipient($email_params->recipient);
        }
        else 
        {
            $mailer->addRecipient($params->email);
        }
        

        $mailer->isHtml(true);
        $mailer->Encoding = 'base64';
        $mailer->setBody($body);

        foreach($attachments as $attachment)
        {
            $mailer->addAttachment($attachment);
        }

        $send = $mailer->Send();

        return true;
    }
}