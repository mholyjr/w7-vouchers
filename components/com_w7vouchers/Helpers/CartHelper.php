<?php

namespace W7Extensions\Component\W7Vouchers\Site\Helpers;

use Joomla\CMS\Factory;
use Joomla\CMS\Component\ComponentHelper;
use Joomla\CMS\Language\Text;

/**
 * @package     Joomla.Site
 * @subpackage  com_w7vouchers
 *
 * @copyright   Copyright (C) 2018 - 2021 W7 Extensions. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

class CartHelper
{

    /**
     * Method to display short description
     * 
     * @param   string  $str
     * @param   int     $length
     * 
     * @return  int
     */
    public static function getCartId(): int
    {
        $app = Factory::getApplication();
        $input = $app->input;
		$id_cart = $input->cookie->get('w7_vouchers_cart', 0);

        return $id_cart;
    }

    public static function setCartCookie($id_cart)
    {
        $app = Factory::getApplication();
        $time = time() + 604800;
        $app->input->cookie->set('w7_vouchers_cart', $id_cart, $time, $app->get('cookie_path', '/'), $app->get('cookie_domain'), $app->isSSLConnection());
    }

    public static function getCartCookie()
    {
        $app = Factory::getApplication();
        $cookie = $app->input->cookie->get('w7_vouchers_cart', 0);

        return $cookie;
    }

    public static function getProducts(int $id_cart)
    {
        $db = Factory::getDbo();
        $query = $db->getQuery(true);

        $query->select('p.*, cp.amount')
            ->from($db->quoteName('#__w7vouchers_carts_products', 'cp'))
            ->join('LEFT', $db->quoteName('#__w7vouchers_items', 'p') . ' ON ' . $db->quoteName('cp.id_product') . ' = ' . $db->quoteName('p.id'))
            ->where($db->quoteName('id_cart') . ' = ' . $id_cart);

        $db->setQuery($query);
        $result = $db->loadObjectList();

        $arr = array();
        $i = 0;
        foreach($result as $row)
        {
            $arr[$i] = $row;
            $arr[$i]->images = \json_decode($row->images);
            $arr[$i]->price = !empty($row->special_price) ? $row->special_price : $row->price;
            $i++;
        }

        return $arr;
    }

    public static function getCartFromOrderId(int $id_order): int
    {
        $db = Factory::getDbo();
        $query = $db->getQuery(true);

        $query->select('o.id_cart')
            ->from($db->quoteName('#__w7vouchers_orders', 'o'))
            ->where($db->quoteName('id') . ' = ' . $id_order);
        $db->setQuery($query);

        $id_cart = $db->loadResult();
        return (int)$id_cart;
    }

    public static function getUserFromOrder(int $id_order): int
    {
        $db = Factory::getDbo();
        $query = $db->getQuery(true);

        $query->select('o.id_user')
            ->from($db->quoteName('#__w7vouchers_orders', 'o'))
            ->where($db->quoteName('id') . ' = ' . $id_order);
        $db->setQuery($query);

        $id_user = $db->loadResult();
        return (int)$id_user;
    }

    public static function getTotalPrice(): int
    {
        $id_cart = self::getCartCookie();

        $db = Factory::getDbo();
        $query = $db->getQuery(true);

        $query
            ->select('c.amount, i.price, i.special_price')
            ->from($db->quoteName('#__w7vouchers_carts_products', 'c'))
            ->join('LEFT', $db->quoteName('#__w7vouchers_items', 'i') . ' ON ' . $db->quoteName('c.id_product') . ' = ' . $db->quoteName('i.id'))
            ->where($db->quoteName('c.id_cart') . ' = :id_cart')
            ->bind(':id_cart', $id_cart);

        $db->setQuery($query);
        $data = $db->loadObjectList();

        $total_price = 0;

        if(count($data))
        {
            foreach($data as $item)
            {
                if(empty($item->special_price)) 
                {
                    $total_price += $item->price * $item->amount;
                }
                else
                {
                    $total_price += $item->special_price * $item->amount;
                }
            }
        }

        return (int)$total_price;
    }

}