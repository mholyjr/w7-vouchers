<?php

namespace W7Extensions\Component\W7Vouchers\Site\Helpers;

/**
 * @package     Joomla.Administrator
 * @subpackage  com_w7vouchers
 *
 * @copyright   Copyright (C) 2018 - 2021 W7 Extensions. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die('Restricted access');

use Joomla\CMS\Factory;
use Joomla\CMS\Response\JsonResponse;
use Joomla\Utilities\ArrayHelper;
use Joomla\CMS\Input\Input;
use Joomla\CMS\Filesystem\File;
use Joomla\CMS\Language\Text;
use W7Extensions\Component\W7Vouchers\Site\Helpers\PdfHelper;
use W7Extensions\Component\W7Vouchers\Site\Helpers\CartHelper;
use W7Extensions\Component\W7Vouchers\Site\Helpers\QrHelper;

class FilesHelper
{

    /**
     * Method to download protected pdf file
     * 
     * @param   string  $filename
     * @param   bool    $validateOwner
     * 
     * @return  mixed   false|void
     */
    public static function downloadVoucher(string $filename, bool $validateOwner = false)
    {
        $app = Factory::getApplication();
        $user = Factory::getUser();

        if ($user->id == 0) {
            $app->enqueueMessage(Text::_('COM_W7VOUCHERS_NOT_AUTHORIZED'), 'error');
            return false;
        }

        if ($validateOwner) {
            $owner = self::getOwner($filename);

            if ($owner != $user->id) {
                $app->enqueueMessage(Text::_('COM_W7VOUCHERS_NOT_AUTHORIZED'), 'error');
                return false;
            }
        }

        $id_order = self::getFolder($filename);

        $file = JPATH_SITE . '/media/com_w7vouchers/pdf/' . $id_order . '/' . $filename . '.pdf';

        if (File::exists($file)) {
            header("Content-type: application/pdf");
            header('Content-Disposition: attachment; filename="' . basename($filename) . '.pdf"');
            readfile($file);
            exit;
        }
    }

    /**
     * Method to get order's folder from filename
     * 
     * @param   string  $filename
     * 
     * @return  int
     */
    private static function getFolder(string $filename): int
    {
        $db = Factory::getDbo();
        $query = $db->getQuery(true);

        $query->select('id_order')
            ->from($db->quoteName('#__w7vouchers_vouchers'))
            ->where($db->quoteName('filename') . ' = :filename')
            ->bind(':filename', $filename);

        $db->setQuery($query);
        $id = $db->loadResult();

        return (int)$id;
    }

    /**
     * Method to get ID of the owner
     * 
     * @param   string  $filename
     * 
     * @return  int
     */
    private static function getOwner(string $filename): int
    {
        $db = Factory::getDbo();
        $query = $db->getQuery(true);

        $query->select('id_user')
            ->from($db->quoteName('#__w7vouchers_vouchers'))
            ->where($db->quoteName('filename') . ' = :filename')
            ->bind(':filename', $filename);

        $db->setQuery($query);
        $id = $db->loadResult();

        return (int)$id;
    }

    /**
     * Method to generate voucher
     * 
     * @param   int     $id_order
     * 
     * @return  void
     */
    public static function createVoucher(int $id_order): void
    {
        $id_cart = CartHelper::getCartFromOrderId($id_order);
        $id_user = CartHelper::getUserFromOrder($id_order);
        $products = CartHelper::getProducts($id_cart);
        $db = Factory::getDbo();

        foreach ($products as $product) {
            for ($x = 1; $x <= $product->amount; $x++) {
                
                $voucher = new \stdClass();

                $code = self::generateRandomString();
                $today = date("Y-m-d H:i:s");
                $valid = date_create($today);
                $add = $product->valid_for . ' ' . $product->valid_for_type;
                date_add($valid, date_interval_create_from_date_string($add));

                $valid = date_format($valid, "Y-m-d H:i:s");

                $voucher->id_order = $id_order;
                $voucher->id_user = $id_user;
                $voucher->expires = $valid;
                $voucher->created = $today;
                $voucher->used = 0;
                $voucher->max_usage = $product->num_of_use;
                $voucher->active = 1;
                $voucher->id_product = $product->id;
                $voucher->title = $product->title;
                $voucher->filename = md5($code);
                $voucher->code = $code;
                $voucher->image = $product->images->voucher_image;
                $voucher->description_short = $product->description_short;
                $voucher->price = !empty($product->special_price) ? $product->special_price : $product->price;

                // create QR code
                $voucher->qr_code = QrHelper::createQrCode($code, $id_order);

                // create PDF
                PdfHelper::createPdf($voucher);

                $db->insertObject('#__w7vouchers_vouchers', $voucher);
            }
        }
    }

    private static function generateRandomString($length = 12) {
		$characters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$charactersLength = strlen($characters);
		$randomString = '';
		for ($i = 0; $i < $length; $i++) {
			$randomString .= $characters[rand(0, $charactersLength - 1)];
		}
		return $randomString;
	}
}
