<?php

namespace W7Extensions\Component\W7Vouchers\Site\Helpers;

/**
 * @package     Joomla.Site
 * @subpackage  com_w7vouchers
 *
 * @copyright   Copyright (C) 2018 - 2021 W7 Extensions. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

use Joomla\CMS\Filesystem\Folder;
use Joomla\CMS\Uri\Uri;

class QrHelper {

    private static $mediaPath = JPATH_SITE . '/images/com_w7vouchers/qr';

    public static function createQrCode(string $code, int $folder): string
    {

        $path = self::$mediaPath . '/' . $folder;
        $file = $path . '/' . $code . ".png";
        $public = Uri::root() . 'images/com_w7vouchers/qr/' . $folder . '/' . $code . '.png';

        if(!Folder::exists($path))
        {
            Folder::create($path);
        }

        \PHPQRCode\QRcode::png($code, $file, 'L', 4, 2);

        return $public;
    }

    public static function createQrCodeSvg()
    {
        $dataText   = $code;
        $svgTagId   = $code;
        $saveToFile = false;
        $imageWidth = 250; // px
        
        // SVG file format support
        $svgCode = \PHPQRCode\QRcode::svg($dataText, $svgTagId, $saveToFile, QR_ECLEVEL_L, $imageWidth);

        return base64_encode($svgCode);
    }

}