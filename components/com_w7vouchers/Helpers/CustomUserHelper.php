<?php

namespace W7Extensions\Component\W7Vouchers\Site\Helpers;

/**
 * @package     Joomla.Administrator
 * @subpackage  com_w7vouchers
 *
 * @copyright   Copyright (C) 2018 - 2021 W7 Extensions. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

use Joomla\CMS\Factory;
use Joomla\CMS\User\UserFactory;
use Joomla\CMS\User\User;
use Joomla\CMS\Plugin\PluginHelper;
use Joomla\CMS\Application\SiteApplication;
use Joomla\CMS\String\PunycodeHelper;
use Joomla\CMS\User\UserHelper;
use Joomla\CMS\Application\ApplicationHelper;

class CustomUserHelper
{

    /**
     * Method to get user ID
     * 
     * @param   string  $email
     * 
     * @return  mixed   int|null
     */
    public static function getID(string $email)
    {
        $db = Factory::getDbo();
        $query = $db->getQuery(true);

        $query->select('id')
            ->from($db->quoteName('#__users'))
            ->where($db->quoteName('email') . ' = :email')
            ->bind(':email', $email);

        $db->setQuery($query);
        $id_user = $db->loadResult();

        return $id_user;
    }

    /**
     * Method to create Joomla user
     * 
     * @return  int     ID of the new user
     */
    public static function addJoomlaUser($username, $first_name, $last_name, $email, $password, $groups, $phone = null, $activate = true)
    {
        jimport('joomla.user.helper');
        
        $groups = is_string($groups) ? explode(',', $groups) : (array) $groups;
        
        $data = [
            'name'   	 => $first_name . ' ' . $last_name,
            'username'	 => $email,
            'password'	 => $password,
            'password2'	 => $password,
            'email'		 => PunycodeHelper::emailToPunycode($email),
            'groups'	 => $groups
        ];
        
        if (!$activate)
        {
            $hash = ApplicationHelper::getHash(UserHelper::genRandomPassword());
            $data['activation'] = $hash;
            $data['block'] = 1;
        }
        
        // Load the users plugin group.
        PluginHelper::importPlugin('user');

        $user = new User;

        $user->defParam('first_name', $first_name);
		$user->defParam('last_name', $last_name);
        $user->defParam('phone', $phone);
        
        if (!$user->bind($data))
        {
            throw new Exception($user->getError());
        }
        
        if (!$user->save())
        {
            throw new Exception($user->getError());
        }
        
        return $user->id;
    }
}