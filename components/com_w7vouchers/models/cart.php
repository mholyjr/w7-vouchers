<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_w7vouchers
 *
 * @copyright   Copyright (C) 2018 - 2021 W7 Extensions. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die('Restricted access');

use Joomla\CMS\Factory;
use Joomla\CMS\Filter\OutputFilter;
use Joomla\CMS\MVC\Model\AdminModel;
use Joomla\CMS\Uri\Uri;
use Joomla\CMS\Plugin\PluginHelper;
use W7Extensions\Component\W7Vouchers\Site\Helpers\DisplayHelper;
use W7Extensions\Component\W7Vouchers\Site\Helpers\CartHelper;

/**
 * Cart Model
 *
 * @since  0.0.1
 */
class W7VouchersModelCart extends AdminModel
{
    /**
     * Method to add product to the cart
     * 
     * @param   array   $product
     * 
     * @return  bool
     */
    public function addToCart(array $product): bool
    {
        PluginHelper::importPlugin('w7vouchers');
        $eventAfter = new Joomla\Event\Event('onAfterAddToCart', [$product]);
        $eventBefore = new Joomla\Event\Event('onBeforeAddToCart', [$product]);

        $app = Factory::getApplication();
        $dispatcher = $app->getDispatcher();
        $user = Factory::getUser();
        $db = $this->getDatabase();
        $id_cart = $app->input->cookie->get('w7_vouchers_cart', null);

        // onBeforeAddToCart Event Dispatcher
        $dispatcher->dispatch('onBeforeAddToCart', $eventBefore);

        if(empty($product['id_product']))
        {
            $app->enqueueMessage(JText::_('COM_W7VOUCHERS_PRODUCT_CANT_BE_EMPTY'), 'error');
            return false;
        }

        if(empty($product['amount']) || $product['amount'] <= 0)
        {
            $product['amount'] = 1;
        }
        
        if ($id_cart == null)
        {
            $cart = new stdClass();
            $cart->finished = 0;
            $cart->id_user = $user->id;

            $db->insertObject('#__w7vouchers_carts', $cart);

            $id_cart = $db->insertid();

            $time = time() + 604800;
            CartHelper::setCartCookie($id_cart);
        }

        $old_amount = $this->checkExisting($id_cart, $product['id_product']);

        if(empty($old_amount))
        {
            $new_product = new stdClass();
            $new_product->id_cart = $id_cart;
            $new_product->id_product = $product['id_product'];
            $new_product->amount = $product['amount'];
    
            $db->insertObject('#__w7vouchers_carts_products', $new_product);
        }
        else
        {
            $amount = $old_amount + $product['amount'];

            $query = $db->getQuery(true);

            $fields = array(
                $db->quoteName('amount') . ' = ' . $amount
            );

            $conditions = array(
                $db->quoteName('id_cart') . ' = ' . $id_cart, 
                $db->quoteName('id_product') . ' = ' . $product['id_product']
            );

            $query->update($db->quoteName('#__w7vouchers_carts_products'))->set($fields)->where($conditions);

            $db->setQuery($query);

            $result = $db->execute();

            // onAfterAddToCart Event Dispatcher
            $dispatcher->dispatch('onAfterAddToCart', $eventAfter);
        }
        
        $this->cleanCache();

        return true;
    }

    /**
     * Method to check if the product is already in the cart
     * 
     * @param   int     $id_cart
     * @param   int     $id_product
     * 
     * @return  mixed
     */
    private function checkExisting(int $id_cart, int $id_product)
    {
        $db = $this->getDatabase();
        $query = $db->getQuery(true);

        $query
            ->select('amount')
            ->from($db->quoteName('#__w7vouchers_carts_products'))
            ->where($db->quoteName('id_product') . ' = ' . $id_product)
            ->where($db->quoteName('id_cart') . ' = ' . $id_cart);

        $db->setQuery($query);
        $amount = $db->loadResult();

        return $amount;
    }

    /**
     * Method to add product to the cart
     * 
     * @param   int     $id_cart
     * 
     * @return  array
     */
    public function getCart(int $id_cart): array
    {
        if(empty($id_cart))
        {
            return [];
        }

        $db = $this->getDatabase();
        $query = $db->getQuery(true);

        $query
            ->select('c.id_product, c.amount, i.title, i.price, i.special_price, i.images, i.description_short')
            ->from($db->quoteName('#__w7vouchers_carts_products', 'c'))
            ->join('LEFT', $db->quoteName('#__w7vouchers_items', 'i') . ' ON ' . $db->quoteName('c.id_product') . ' = ' . $db->quoteName('i.id'))
            ->where($db->quoteName('c.id_cart') . ' = :id_cart')
            ->bind(':id_cart', $id_cart);

        $db->setQuery($query);
        $data = $db->loadObjectList();

        $arr = array();
        $i = 0;
        $cart_total_price = 0;
        $cart_total_items = 0;

        if(count($data))
        {

            foreach($data as $item)
            {
                $arr['products'][$i] = $item;
                $images = json_decode($item->images);
                $preparedImage = '';
                $price = 0;
                if(!empty($item->special_price))
                {
                    $price = $item->special_price;
                }
                else
                {
                    $price = $item->price;
                }

                foreach($images as $image)
                {
                    if(empty($image->intro_image))
                    {
                        $preparedImage = Uri::root() . '/components/com_w7vouchers/assets/images/empty.jpg';
                    }
                    else
                    {
                        $preparedImage = $image->intro_image;
                    }
                }

                $cart_total_price += $price * $item->amount;
                $cart_total_items += $item->amount;

                $arr['products'][$i]->image = $preparedImage;
                $arr['products'][$i]->price = DisplayHelper::displayPrice($price * $item->amount);

                $i++;
            }

            $arr['total_price'] = DisplayHelper::displayPrice($cart_total_price);
            $arr['total_items'] = $cart_total_items;
        }
        else
        {
            $arr['products'] = array();
        }
        

        return $arr;

    }

    /**
     * Method to remove item from a cart
     * 
     * @param   array   $data
     * 
     * @return  bool
     */
    public function removeItem(array $data): bool
    {

        $app = Factory::getApplication();
        $id_cart = CartHelper::getCartId();

        if(empty($id_cart))
        {
            $app->enqueueMessage(JText::_('COM_W7VOUCHERS_PRODUCT_CANT_BE_EMPTY'), 'error');
            return false;
        }

        $db = $this->getDatabase();
        $query = $db->getQuery(true);

        $conditions = array(
            $db->quoteName('id_product') . ' = ' . $data['id_product'],
            $db->quoteName('id_cart') . ' = ' . $id_cart
        );
        $query->delete($db->quoteName('#__w7vouchers_carts_products'));
        $query->where($conditions);

        $db->setQuery($query);

        $result = $db->execute();
        return true;
    }


    /**
	 * Method to get the record form.
	 *
	 * @param   array    $data      Data for the form.
	 * @param   boolean  $loadData  True if the form is to load its own data (default case), false if not.
	 *
	 * @return  mixed    A JForm object on success, false on failure
	 *
	 * @since   1.6
	 */
	public function getForm($data = array(), $loadData = true)
	{
		
	}
}