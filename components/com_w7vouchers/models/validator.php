<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_w7vouchers
 *
 * @copyright   Copyright (C) 2018 - 2021 W7 Extensions. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die('Restricted access');

use Joomla\CMS\Factory;
use Joomla\CMS\Filter\OutputFilter;
use Joomla\CMS\MVC\Model\AdminModel;
use Joomla\Registry\Registry;

/**
 * Validator Model
 *
 * @since  0.0.1
 */
class W7VouchersModelValidator extends AdminModel
{
    /**
     * Model context string.
     *
     * @var        string
     */
    protected $_context = 'com_w7vouchers.validator';

    /**
     * Method to auto-populate the model state.
     *
     * Note. Calling getState in this method will result in recursion.
     *
     * @since   1.6
     *
     * @return void
     */
    protected function populateState()
    {
        $app = Factory::getApplication();

        // Load state from the request.
        $pk = $app->input->get('code', 'STRING', '');
        $this->setState('item.code', $pk);

        // Load the parameters.
        $params = $app->getParams();
        $this->setState('params', $params);
    }

    /**
     * Method to get article data.
     *
     * @param   integer  $pk  The id of the article.
     *
     * @return  object|boolean  Menu item data object on success, boolean false
     */
    public function getItem($pk = null)
    {
        $pk = $this->getState('item.code');

        $db = $this->getDatabase();
        $query = $db->getQuery(true);

        $query->select('i.*, p.title as product_title, o.first_name, o.last_name, o.email')
            ->from($db->quoteName('#__w7vouchers_vouchers', 'i'))
            ->join('LEFT', $db->quoteName('#__w7vouchers_items', 'p') . ' ON ' . $db->quoteName('i.id_product') . ' = ' . $db->quoteName('p.id'))
            ->join('LEFT', $db->quoteName('#__w7vouchers_orders', 'o') . ' ON ' . $db->quoteName('i.id_order') . ' = ' . $db->quoteName('o.id'))
            ->where($db->quoteName('i.code') . ' = :code')
            ->bind(':code', $pk);

        $db->setQuery($query);

        $data = $db->loadObject();

        if(!$this->checkValidity($data) && $data->active == 1)
        {
            $this->getItem();
        }

        return $data;

        parent::cleanCache('com_w7vouchers');
    }

    /**
	 * Method to get the data that should be injected in the form.
	 *
	 * @return  mixed  The data for the form.
	 *
	 * @since   1.6
	 */
	protected function loadFormData()
	{
		$data = JFactory::getApplication()->getUserState(
			'com_w7vouchers.validator',
			array()
		);

		if (empty($data))
		{
			$data = $this->getItem();
		}

		return $data;
	}

    /**
     * Method to check validity of the voucher
     * 
     * @return bool
     */
    private function checkValidity(object $voucher): bool
    {
        $db = Factory::getDbo();
        $today = date("Y-m-d H:i:s");
        $date = $voucher->expires;

        $prep_obj = new StdClass();
        $prep_obj->id = $voucher->id;
        $prep_obj->active = 0;

        if ($date < $today) 
        {
            $db->updateObject('#__w7vouchers_vouchers', $prep_obj, 'id');
            return false;
        }

        if($voucher->used >= $voucher->max_usage)
        {
            $db->updateObject('#__w7vouchers_vouchers', $prep_obj, 'id');
            return false;
        }

        return true;

        parent::cleanCache('com_w7vouchers');
    }

    /**
	 * Method to get the record form.
	 *
	 * @param   array    $data      Data for the form.
	 * @param   boolean  $loadData  True if the form is to load its own data (default case), false if not.
	 *
	 * @return  mixed    A JForm object on success, false on failure
	 *
	 * @since   1.6
	 */
	public function getForm($data = array(), $loadData = true)
	{

		$form = $this->loadForm(
			'com_w7vouchers.validator',
			'validator',
			array(
				'control' => 'jform',
				'load_data' => $loadData
			)
            );

		return $form;
	}

    /**
     * Method to log voucher redeem
     * 
     * @param   string  $code
     * 
     * @return  bool
     */
    public function logUsage(string $code): bool
    {
        $db = Factory::getDbo();

        $query = $db->getQuery(true)
			->update($db->quoteName('#__w7vouchers_vouchers'))
			->set($db->quoteName('used') . ' = (' . $db->quoteName('used') . ' + 1)')
            ->set($db->quoteName('validated') . ' = "' . date("Y-m-d H:i:s") . '"')
            ->where($db->quoteName('code') . ' = :code')
            ->bind(':code', $code);

		$db->setQuery($query);
		$db->execute();

        return true;

    }
	

}