<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_w7vouchers
 *
 * @copyright   Copyright (C) 2018 - 2021 W7 Extensions. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die('Restricted access');

use Joomla\CMS\Factory;

/**
 * Category Model
 *
 * @since  0.0.1
 */
class W7VouchersModelCategory extends JModelList
{

    /**
	 * Method to auto-populate the model state.
	 *
	 * Note. Calling getState in this method will result in recursion.
	 *
	 * @param   string  $ordering   An optional ordering field.
	 * @param   string  $direction  An optional direction (asc|desc).
	 *
	 * @return  void
	 *
	 * @since   1.6
	 * @throws  \Exception
	 */
	protected function populateState($ordering = 'i.id', $direction = 'desc')
	{
        $app = Factory::getApplication();
        $this->params = $app->getParams();

        // Get pagination request variables
        $limit = $app->getUserStateFromRequest('global.list.limit', 'limit', $this->params->get('list_limit'), 'int');
        $limitstart = $app->input->get('limitstart', 0, 'int');

        $id = $app->input->get('id', 0, 'int');
        $this->setState('id', $id);

        // In case limit has been changed, adjust it
        $limitstart = ($limit != 0 ? (floor($limitstart / $limit) * $limit) : 0);

        $this->setState('limit', $limit);
        $this->setState('limitstart', $limitstart);

		// List state information.
		parent::populateState($ordering, $direction);
	}

    /**
     * Get Category items
     * 
     * @return array Breaches items
     */
    public function getItems()
    {

        $itemsQ = $this->getListQuery();
        $items = $this->_getList($itemsQ, $this->getState('limitstart'), $this->getState('limit'));	

        $this->params = JFactory::getApplication()->getParams();

        return $items;
    }

    /**
	 * Method to get a JPagination object for the data set.
	 *
	 * @return  JPagination  A JPagination object for the data set.
	 *
	 * @since   3.0.1
	 */
	function getPagination()
    {
        // Load the content if it doesn't already exist
        if (empty($this->_pagination)) {
            jimport('joomla.html.pagination');
            $this->_pagination = new JPagination($this->getTotal(), $this->getState('limitstart'), $this->getState('limit') );
        }
        return $this->_pagination;
    }

    /**
	 * Method to build an SQL query to load the list data.
	 *
	 * @return      string  An SQL query
	 */
	protected function getListQuery()
	{
        $db    = Factory::getDbo();
		$query = $db->getQuery(true);

        $id_user = $this->getState('id_user');

        $query->select('i.*, c.alias as catslug, c.title as cat_title')
            ->from($db->quoteName('#__w7vouchers_items', 'i'))
            ->join('LEFT', $db->quoteName('#__w7vouchers_categories', 'c') . ' ON ' . $db->quoteName('i.catid') . ' = ' . $db->quoteName('c.id'))
            ->where($db->quoteName('i.published') . ' = 1')
            ->where($db->quoteName('c.published') . ' = 1');

        return $query;
    }

    public function getTitle()
    {
        $id = $this->getState('id');
        $db    = Factory::getDbo();
		$query = $db->getQuery(true);

        $query->select('c.title')
            ->from($db->quoteName('#__w7vouchers_categories', 'c'))
            ->where($db->quoteName('c.id') . ' = :id')
            ->bind(':id', $id);

        $db->setQuery($query);
        $title = $db->loadResult();
       
        return $title;
    }

    public function getTotal()
    {
       // Load the content if it doesn't already exist
       if (empty($this->_total)) {
           $query = $this->getListQuery();
           $this->_total = $this->_getListCount($query);	
       }
       return $this->_total;
    }
}