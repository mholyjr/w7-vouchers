<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_w7vouchers
 *
 * @copyright   Copyright (C) 2018 - 2021 W7 Extensions. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die('Restricted access');

use Joomla\CMS\Factory;
use Joomla\CMS\Filter\OutputFilter;
use Joomla\CMS\MVC\Model\ItemModel;
use Joomla\Registry\Registry;

/**
 * Item Model
 *
 * @since  0.0.1
 */
class W7VouchersModelItem extends ItemModel
{
    /**
     * Model context string.
     *
     * @var        string
     */
    protected $_context = 'com_w7vouchers.item';

    /**
     * Method to auto-populate the model state.
     *
     * Note. Calling getState in this method will result in recursion.
     *
     * @since   1.6
     *
     * @return void
     */
    protected function populateState()
    {
        $app = Factory::getApplication();

        // Load state from the request.
        $pk = $app->input->getInt('id');
        $this->setState('item.id', $pk);

        // Load the parameters.
        $params = $app->getParams();
        $this->setState('params', $params);
    }

    /**
     * Method to get article data.
     *
     * @param   integer  $pk  The id of the article.
     *
     * @return  object|boolean  Menu item data object on success, boolean false
     */
    public function getItem($pk = null)
    {
        $pk = (int) ($pk ?: $this->getState('item.id'));

        $db = Factory::getDbo();
        $query = $db->getQuery(true);

        $query->select('i.*, c.title as cat_title')
            ->from($db->quoteName('#__w7vouchers_items', 'i'))
            ->join('LEFT', $db->quoteName('#__w7vouchers_categories', 'c') . ' ON ' . $db->quoteName('i.catid') . ' = ' . $db->quoteName('c.id'))
            ->where($db->quoteName('i.id') . ' = :id')
            ->where($db->quoteName('i.published') . ' = 1')
            ->bind(':id', $pk);

        $db->setQuery($query);

        $data = $db->loadObject();

        if(!empty($data)) 
		{
			$params = new JRegistry;
			$params->loadString($data->params, 'JSON');
			$data->params = $params;

			$params = clone $this->getState('params');
			$params->merge($data->params);
			$data->params = $params;
		}

        $images = new Registry($data->images);
		$data->images = $images->toArray();

        $stores = new Registry($data->stores);
        $data->stores = $stores->toArray();

        $i = 0;
        foreach($data->stores as $store)
        {
            $query = $db->getQuery(true);
            $query->select('*')
                ->from($db->quoteName('#__w7vouchers_stores'))
                ->where($db->quoteName('id') . ' = :id')
                ->bind(':id', $store);

            $db->setQuery($query);
            $store_data = $db->loadObject();

            $data->stores[$i] = $store_data;

            $i++;
        }

        return $data;
    }
	

}