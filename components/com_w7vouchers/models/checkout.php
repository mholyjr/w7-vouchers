<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_w7vouchers
 *
 * @copyright   Copyright (C) 2018 - 2021 W7 Extensions. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die('Restricted access');

use Joomla\CMS\Factory;
use Joomla\CMS\Filter\OutputFilter;
use Joomla\CMS\MVC\Model\AdminModel;
use Joomla\CMS\Uri\Uri;
use Joomla\Utilities\ArrayHelper;
use Joomla\CMS\Object\CMSObject;
use Joomla\CMS\Router\Route;
use W7Extensions\Component\W7Vouchers\Site\Helpers\DisplayHelper;
use W7Extensions\Component\W7Vouchers\Site\Helpers\CartHelper;
use W7Extensions\Component\W7Vouchers\Site\Helpers\CustomUserHelper;
use W7Extensions\Component\W7Vouchers\Site\Helpers\PdfHelper;
use W7Extensions\Component\W7Vouchers\Site\Helpers\EmailsHelper;
use W7Extensions\Component\W7Vouchers\Site\Helpers\FilesHelper;
use W7Extensions\Component\W7Vouchers\Site\Helpers\QrHelper;

/**
 * Checkout Model
 *
 * @since  0.0.1
 */
class W7VouchersModelCheckout extends AdminModel
{

	/**
	 * Method to override getItem to allow us to convert the JSON-encoded image information
	 * in the database record into an array for subsequent prefilling of the edit form
	 */
	public function getItem($pk = null)
	{
		$user = Factory::getUser();

		if(empty($user->id))
		{
			$properties = [];
		}
		else
		{
			$properties = ['email' => $user->get('email'), 'first_name' => $user->getParam('first_name'), 'last_name' => $user->getParam('last_name')];
		}
		

		$item = ArrayHelper::toObject($properties, CMSObject::class);

		return $item; 
	}

	/**
	 * Method to get the data that should be injected in the form.
	 *
	 * @return  mixed  The data for the form.
	 *
	 * @since   1.6
	 */
	protected function loadFormData()
	{
		$data = JFactory::getApplication()->getUserState(
			'com_w7vouchers.checkout',
			array()
		);

		if (empty($data))
		{
			$data = $this->getItem();
		}

		return $data;
	}

	/**
	 * Method to get form fields
	 * 
	 * @return	array
	 */
	private function getFormFields()
	{
		$db = $this->getDatabase();
		$query = $db->getQuery(true);

		$query->select('*')
			->from($db->quoteName('#__w7vouchers_fields'))
			->where($db->quoteName('published') . ' = 1')
			->order($db->escape('ordering') . ' ASC');

		$db->setQuery($query);

		$fields = $db->loadObjectList();

		return $fields;
	}

	/**
	 * Method to get payment methods
	 * 
	 * @return	array
	 */
	private function getPaymentMethods()
	{
		$db = $this->getDatabase();
		$query = $db->getQuery(true);

		$query->select('*')
			->from($db->quoteName('#__w7vouchers_payment_methods'))
			->where($db->quoteName('published') . ' = 1');

		$db->setQuery($query);

		$fields = $db->loadObjectList();

		return $fields;
	}

    /**
	 * Method to get the record form.
	 *
	 * @param   array    $data      Data for the form.
	 * @param   boolean  $loadData  True if the form is to load its own data (default case), false if not.
	 *
	 * @return  mixed    A JForm object on success, false on failure
	 *
	 * @since   1.6
	 */
	public function getForm($data = array(), $loadData = true)
	{

		$user = Factory::getUser();
		$fields = $this->getFormFields();
		$paymentMethods = $this->getPaymentMethods();

		$form = $this->loadForm(
			'com_w7vouchers.checkout',
			'checkout',
			array(
				'control' => 'jform',
				'load_data' => $loadData
			)
		);

		foreach($fields as $field)
		{
			if($field->type != 'select')
			{

				if(!empty($user->id) && $field->type == 'password')
				{
					continue;
				}

				$field_xml = new SimpleXMLElement('
					<field
						name="'.$field->name.'"
						type="'.$field->type.'"
						label="'.$field->label.'"
						required="'.DisplayHelper::trueFalse($field->required).'"
					/>
				');

				$form->setField($field_xml, null, true, 'custom');
			}
		}

		$payment_field = '<field
				name="payment_method"
				type="list"
				label="COM_W7VOUCHERS_PAYMENT_METHOD"
			>';

		foreach($paymentMethods as $method)
		{
			$payment_field .= '<option value="'.$method->service.'">'.$method->title.'</option>';
		}

		$payment_field .= '</field>';

		$field_xml = new SimpleXMLElement($payment_field);
		$form->setField($field_xml, null, true, 'custom');

		if(!empty($user->id))
		{
			$form->removeField('email');

			$new_email_field = new SimpleXMLElement('
					<field
						name="email"
						type="hidden"
					/>
			');
			$form->setField($new_email_field, null, true, 'main');
		}

		return $form;
	}

	/**
	 * Method to prepare order
	 * 
	 * @param	array	$data
	 * @param	int		$id_cart
	 * 
	 * @return	bool
	 */
	public function prepareOrder(array $data, int $id_cart): bool
	{
		$db = Factory::getDbo();
		$data = (object)$data;
		$user = Factory::getUser();
		$app = Factory::getApplication();
		$params = $app->getParams();

		if(!empty($user->id))
		{
			$data->id_user = $user->id;
			$user->setParam('first_name', $data->first_name);
			$user->setParam('last_name', $data->last_name);
			$user->setParam('phone', $data->phone);
			$user->save();
		}
		else
		{
			$existing = CustomUserHelper::getID($data->email);

			if(empty($existing))
			{
				$data->id_user = CustomUserHelper::addJoomlaUser(
					$data->email, 
					$data->first_name,
					$data->last_name, 
					$data->email, 
					$data->password, 
					(string)$params->get('customer_usergroup', '2'), 
					true
				);
			}
			else
			{
				$data->id_user = $existing;
				$user->setParam('first_name', $data->first_name);
				$user->setParam('last_name', $data->last_name);
				$user->setParam('phone', $data->phone ? $data->phone : null);
				$user->save();
			}
			
		}

		// Create order record
		$data->id_cart = (int)$id_cart;
		$data->paid = 0;
		$data->published = 1;
		$data->state = $params->get('state_before', 1);
		$data->created = date("Y-m-d H:i:s");
		$data->total_price = CartHelper::getTotalPrice();
		$payment_method = $data->custom_fields['payment_method'];

		// Handle password
		unset($data->custom_fields['password']);
		$data->custom_fields = json_encode($data->custom_fields);

		$db->insertObject('#__w7vouchers_orders', $data);
		$id_order = $db->insertid();

		// Update cart record
		$cart = new stdClass();
		$cart->id = $id_cart;
		$cart->finished = 1;
		$cart->id_user = $data->id_user;

		$db->updateObject('#__w7vouchers_carts', $cart, 'id');

		// Set cart cookie to null
		CartHelper::setCartCookie(null);

		switch($payment_method)
		{
			case 'Stripe':
				$this->stripeCharges((int)$id_order);
			break;
			case 'Offline':
				header('Location: '.Route::_('index.php?option=com_w7vouchers&view=confirm', false));exit;
			break;
		}

		return true;
	}

	/**
	 * Method to set order after payment
	 * 
	 * @param	int		$id_order
	 * 
	 * @return	bool
	 */
	public function setOrder(int $id_order)
	{
		$db = Factory::getDbo();
		$app = Factory::getApplication();
		$params = $app->getParams();
		$order = new StdClass();
		$id_cart = CartHelper::getCartFromOrderId($id_order);
		$id_user = CartHelper::getUserFromOrder($id_order);
		$data = $this->getOrder($id_order);
		$products = CartHelper::getProducts($id_cart);

		if($data->order->generated == 0)
		{
			$order->id = $id_order;
			$order->state = $params->get('state_after', 2);
			$order->paid = 1;
			$order->generated = 1;
			$db->updateObject('#__w7vouchers_orders', $order, 'id');

			FilesHelper::createVoucher($id_order);
	
			$this->handleConfirmEmail($id_order, $data->order);
			EmailsHelper::sendEmail($data->order, 'admin-notification');
		}
	
		return true;
	}

	/**
	 * Stripe payment
	 * 
	 * @param	int		$id_order
	 * 
	 * @return 	void
	 */
	public function stripeCharges(int $id_order): void
    {
		$app = Factory::getApplication();
		$params = $app->getParams();
        $order = $this->getOrder($id_order);
        $stripe_opts = $this->getPayment('Stripe');

        foreach ($order->products as $product) 
		{
			$product_price = empty($product->special_price) ? $product->price : $product->special_price;
			$product_price = round($product_price, 2);
			
            $line_item = [
				'price_data' => [
					'currency' => $params->get('currency_code'),
					'product_data' => [
						'name' => $product->title,
					],
					'unit_amount' => $product_price * 100,
				],
				'quantity' => $product->amount
			];
			$array['line_items'][] = $line_item;
        }

		$stripe = new \Stripe\StripeClient($stripe_opts->secret_key);

		$res = $stripe->checkout->sessions->create([
			'line_items' => $array['line_items'],
			'mode' => 'payment',
			'success_url' => JUri::root()."index.php?option=com_w7vouchers&task=checkout.setOrder&id_order=".$id_order,
            'cancel_url' => JUri::root()
		]);

		$this->updateTransactionId($id_order, (string)$res->payment_intent);
		header('Location: '.$res->url);exit;
    }

	/**
	 * Method to update transaction ID
	 * 
	 * @param	int		$id_order
	 * @param	string	$id_transaction
	 * 
	 * @return bool
	 */
	public function updateTransactionId(int $id_order, string $id_transaction)
	{
		$db = Factory::getDbo();
		
		$order = new stdClass();
		$order->id = $id_order;
		$order->transaction_id = $id_transaction;

		$db->updateObject('#__w7vouchers_orders', $order, 'id');
	}

	/**
	 * Method to get order object
	 * 
	 * @param	int		$id_order
	 * 
	 * @return	object
	 */
	public function getOrder(int $id_order)
	{
		$db = Factory::getDbo();
		$query = $db->getQuery(true);

		$query->select('o.*')
			->from($db->quoteName('#__w7vouchers_orders', 'o'))
			->where($db->quoteName('id') . ' = :id_order')
			->bind(':id_order', $id_order);

		$db->setQuery($query);
		$order_data = $db->loadObject();

		$query = $db->getQuery(true);
		$query->select('c.amount, i.title, i.price, i.special_price')
			->from($db->quoteName('#__w7vouchers_carts_products', 'c'))
			->join('LEFT', $db->quoteName('#__w7vouchers_items', 'i') . ' ON ' . $db->quoteName('c.id_product') . ' = ' . $db->quoteName('i.id'))
			->where($db->quoteName('c.id_cart') . ' = :id_cart')
			->bind(':id_cart', $order_data->id_cart);

		$db->setQuery($query);
		$products_data = $db->loadObjectList();

		$order = new stdClass();
		$order->order = $order_data;
		$order->products = $products_data;

		return $order;
	}

	/**
	 * Method to get payment method data
	 * 
	 * @param	string	$service
	 * 
	 * @return	object
	 */
	public function getPayment(string $service)
	{
		$db = Factory::getDbo();
		$query = $db->getQuery(true);

		$query->select('*')
			->from($db->quoteName('#__w7vouchers_payment_methods'))
			->where($db->quoteName('service') . ' = :service')
			->bind(':service', $service);

		$db->setQuery($query);
		$payment = $db->loadObject();

		if($payment->production === 1)
		{
			$res = json_decode($payment->production_settings);
		}
		else
		{
			$res = json_decode($payment->test_settings);
		}

		return (object)$res;

	}

	private function handleConfirmEmail(int $id_order, object $params)
	{
		$db = Factory::getDbo();
		$query = $db->getQuery(true);

		$query->select('filename')
			->from($db->quoteName('#__w7vouchers_vouchers'))
			->where($db->quoteName('id_order') . ' = :id_order')
			->bind(':id_order', $id_order);
		$db->setQuery($query);
		$files = $db->loadColumn();

		$arr = array();
		$i = 0;
		foreach($files as $file)
		{
			$arr[$i] = JPATH_SITE . '/media/com_w7vouchers/pdf/' . $id_order . '/' . $file . '.pdf';
			$i++;
		}

		$attachments = $arr;

		EmailsHelper::sendEmail($params, 'customer-confirm', $attachments);
	}

	private function createVoucher(int $id_user, object $product, int $id_order): void
	{
		$db = Factory::getDbo();
		$voucher = new stdClass();

		$code = $this->generateRandomString();
		$today = date("Y-m-d H:i:s");
		$valid = date_create($today);
		$add = $product->valid_for . ' ' . $product->valid_for_type;
		date_add($valid, date_interval_create_from_date_string($add));

		$valid = date_format($valid,"Y-m-d H:i:s");

		$voucher->id_order = $id_order;
		$voucher->id_user = $order->id_user;
		$voucher->expires = $valid;
		$voucher->created = $today;
		$voucher->used = 0;
		$voucher->max_usage = $product->num_of_use;
		$voucher->active = 1;
		$voucher->id_product = $product->id;
		$voucher->title = $product->title;
		$voucher->filename = md5($code);
		$voucher->code = $code;
		$voucher->price = !empty($product->special_price) ? $product->special_price : $product->price;

		// create QR code
		$voucher->qr_code = QrHelper::createQrCode($code, $id_order);

		// create PDF
		PdfHelper::createPdf($voucher);

		$db->insertObject('#__w7vouchers_vouchers', $voucher);
	}

	private function generateRandomString($length = 12) {
		$characters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$charactersLength = strlen($characters);
		$randomString = '';
		for ($i = 0; $i < $length; $i++) {
			$randomString .= $characters[rand(0, $charactersLength - 1)];
		}
		return $randomString;
	}

	private function encode($arr, $prefix = null)
    {
        if (!is_array($arr)) {
            return $arr;
        }
        $r = array();
        foreach ($arr as $k => $v) {
            if (is_null($v)) {
                continue;
            }
            if ($prefix && $k && !is_int($k)){
                $k = $prefix."[".$k."]";
            } else if ($prefix) {
                $k = $prefix."[]";
            }
            if (is_array($v)) {
                $r[] = $this->encode($v, $k, true);
            } else {
                $r[] = urlencode($k)."=".urlencode($v);
            }
        }

        return implode("&", $r);
    }
}