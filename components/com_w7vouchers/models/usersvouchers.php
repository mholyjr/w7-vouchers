<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_w7vouchers
 *
 * @copyright   Copyright (C) 2018 - 2021 W7 Extensions. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die('Restricted access');

use Joomla\CMS\Factory;
use Joomla\Database\ParameterType;

/**
 * Users Vouchers Model
 *
 * @since  0.0.1
 */
class W7VouchersModelUsersVouchers extends JModelList
{

    /**
	 * Method to auto-populate the model state.
	 *
	 * Note. Calling getState in this method will result in recursion.
	 *
	 * @param   string  $ordering   An optional ordering field.
	 * @param   string  $direction  An optional direction (asc|desc).
	 *
	 * @return  void
	 *
	 * @since   1.6
	 * @throws  \Exception
	 */
	protected function populateState($ordering = 'i.id', $direction = 'desc')
	{
        $app = Factory::getApplication();
        $user = Factory::getUser();
        $this->params = $app->getParams();

        // Get pagination request variables
        $limit = $app->getUserStateFromRequest('global.list.limit', 'limit', $this->params->get('list_limit'), 'int');
        $limitstart = $app->input->get('limitstart', 0, 'int'); 

        // In case limit has been changed, adjust it
        $limitstart = ($limit != 0 ? (floor($limitstart / $limit) * $limit) : 0);

        $this->setState('limit', $limit);
        $this->setState('limitstart', $limitstart);
        $this->setState('id_user', $user->id);

		// List state information.
		parent::populateState($ordering, $direction);
	}

    /**
     * Get Vouchers items
     * 
     * @return array Breaches items
     */
    public function getItems()
    {

        $itemsQ = $this->getListQuery();
        $items = $this->_getList($itemsQ, $this->getState('limitstart'), $this->getState('limit'));	

        $this->params = JFactory::getApplication()->getParams();

        return $items;
    }

    /**
	 * Method to get a JPagination object for the data set.
	 *
	 * @return  JPagination  A JPagination object for the data set.
	 *
	 * @since   3.0.1
	 */
	function getPagination()
    {
        // Load the content if it doesn't already exist
        if (empty($this->_pagination)) {
            jimport('joomla.html.pagination');
            $this->_pagination = new JPagination($this->getTotal(), $this->getState('limitstart'), $this->getState('limit') );
        }
        return $this->_pagination;
    }

    /**
	 * Method to build an SQL query to load the list data.
	 *
	 * @return      string  An SQL query
	 */
	protected function getListQuery()
	{
        $db    = Factory::getDbo();
		$query = $db->getQuery(true);

        $id_user = $this->getState('id_user');

        if($id_user !== 0)
        {
            $query->select('i.*, p.title')
                ->from($db->quoteName('#__w7vouchers_vouchers', 'i'))
                ->join('LEFT', $db->quoteName('#__w7vouchers_items', 'p') . ' ON ' . $db->quoteName('i.id_product') . ' = ' . $db->quoteName('p.id'))
                ->where($db->quoteName('i.id_user') . ' = :id_user')
                ->order($db->quoteName('i.active') . ' DESC')
                ->bind(':id_user', $id_user, ParameterType::INTEGER);

            return $query;
        }
        else 
        {
            return false;
        }
        
    }

    public function getTotal()
    {
       // Load the content if it doesn't already exist
       if (empty($this->_total)) {
           $query = $this->getListQuery();
           $this->_total = $this->_getListCount($query);	
       }
       return $this->_total;
    }
}