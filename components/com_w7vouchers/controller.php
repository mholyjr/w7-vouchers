<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_w7vouchers
 *
 * @copyright   Copyright (C) 2005 - 2018 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access to this file
defined('_JEXEC') or die('Restricted access');

use Joomla\CMS\Factory;

/**
 * W7 Vouchers Component Controller
 *
 * @since  0.0.1
 */
class W7VouchersController extends JControllerLegacy
{
}