<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_w7vouchers
 *
 * @copyright   Copyright (C) 2018 - 2021 W7 Extensions. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access to this file
defined('_JEXEC') or die('Restricted access');

require 'components/com_w7vouchers/vendor/autoload.php';

$document = JFactory::getDocument();

$document->addStylesheet('components/com_w7vouchers/assets/css/styles.css');
$document->addStylesheet('components/com_w7vouchers/assets/css/bootstrap.min.css');
$document->addScript('components/com_w7vouchers/assets/js/bootstrap.min.js');


$controller = JControllerLegacy::getInstance('W7Vouchers');

$controller->execute(JFactory::getApplication()->input->get('task'));

$controller->redirect();