DROP TABLE IF EXISTS `#__w7vouchers_carts`;

CREATE TABLE `#__w7vouchers_carts` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `id_user` int(11) DEFAULT NULL,
  `finished` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `#__w7vouchers_carts_products`;

CREATE TABLE `#__w7vouchers_carts_products` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `id_cart` int(11) DEFAULT NULL,
  `id_product` int(11) DEFAULT NULL,
  `amount` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `#__w7vouchers_categories`;

CREATE TABLE `#__w7vouchers_categories` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(1024) NOT NULL DEFAULT '',
  `description_short` text NOT NULL,
  `description_full` text NOT NULL,
  `published` int(1) NOT NULL,
  `created` datetime NOT NULL,
  `updated` datetime NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `alias` varchar(1024) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  UNIQUE KEY `alias` (`alias`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `#__w7vouchers_emails`;

CREATE TABLE `#__w7vouchers_emails` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(1024) NOT NULL DEFAULT '',
  `subject` varchar(1024) DEFAULT '',
  `content` text NOT NULL,
  `type` varchar(256) NOT NULL DEFAULT '',
  `params` text,
  `usertype` varchar(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `#__w7vouchers_emails` (`id`, `title`, `subject`, `content`, `type`, `params`, `usertype`)
VALUES
	(1,'COM_W7VOUCHERS_ORDER_CONFIRM_EMAIL','Vaše zakoupené vouchery','<p>Dobrý den, [FIRST_NAME] [LAST_NAME], </p>\r\n<p>děkujeme za nákup našich voucherů. Všechny Vaše vouchery naleznete jak v příloze tohoto emailu, tak po přihlášení v klientské sekci. </p>\r\n<p> </p>\r\n<p>S pozdravem,</p>\r\n<p>[COMPANY_NAME]</p>','customer-confirm',NULL,'user'),
	(2,'COM_W7VOUCHERS_ORDER_NOTIFICATION_EMAIL','Máte novou objednávku','<p>Dobrý den,</p>\r\n<p>máte novou objednávku voucherů</p>','admin-notification','{\"recipient\":\"info@web7.cz\",\"send\":\"1\"}','admin');

DROP TABLE IF EXISTS `#__w7vouchers_fields`;

CREATE TABLE `#__w7vouchers_fields` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `label` varchar(1024) NOT NULL,
  `type` varchar(1024) NOT NULL,
  `name` varchar(1024) NOT NULL,
  `core` int(1) NOT NULL,
  `required` int(1) NOT NULL,
  `published` int(1) NOT NULL,
  `options` text NOT NULL,
  `ordering` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `#__w7vouchers_fields` (`id`, `label`, `type`, `name`, `core`, `required`, `published`, `options`, `ordering`)
VALUES
	(1,'E-mail','email','email',0,1,1,'',3),
	(2,'COM_W7VOUCHERS_FIRST_NAME','text','first_name',1,1,1,'',1),
	(3,'COM_W7VOUCHERS_LAST_NAME','text','last_name',1,1,1,'',2),
	(4,'Password','password','password',1,1,1,'',4),
	(5,'Note','textarea','note',0,0,1,'',6),
	(6,'COM_W7VOUCHERS_PHONE','text','phone',0,1,1,'',5),
	(7,'','','',0,0,0,'',0);

DROP TABLE IF EXISTS `#__w7vouchers_items`;

CREATE TABLE `#__w7vouchers_items` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `catid` int(11) NOT NULL,
  `title` varchar(255) NOT NULL DEFAULT '',
  `alias` varchar(255) NOT NULL,
  `description_full` text NOT NULL,
  `description_short` text NOT NULL,
  `num_of_use` int(11) NOT NULL DEFAULT '1',
  `valid_for` int(11) NOT NULL,
  `valid_for_type` varchar(255) NOT NULL DEFAULT 'months',
  `price` int(11) NOT NULL DEFAULT '0',
  `special_price` int(11) DEFAULT NULL,
  `created` datetime NOT NULL DEFAULT '1970-01-01 00:00:00',
  `created_by` int(11) NOT NULL DEFAULT '0',
  `updated` datetime NOT NULL DEFAULT '1970-01-01 00:00:00',
  `updated_by` int(11) NOT NULL DEFAULT '0',
  `published` int(1) NOT NULL DEFAULT '1',
  `images` text NOT NULL,
  `price_note` varchar(1024) NOT NULL DEFAULT '',
  `stores` varchar(1024) DEFAULT NULL,
  `params` text,
  PRIMARY KEY (`id`),
  UNIQUE KEY `alias` (`alias`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `#__w7vouchers_orders`;

CREATE TABLE `#__w7vouchers_orders` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `id_user` int(11) NOT NULL,
  `id_cart` int(11) NOT NULL,
  `paid` int(11) NOT NULL DEFAULT '0',
  `created` datetime NOT NULL,
  `published` int(1) NOT NULL DEFAULT '1',
  `state` int(11) NOT NULL DEFAULT '1',
  `first_name` varchar(1024) NOT NULL DEFAULT '',
  `last_name` varchar(1024) NOT NULL DEFAULT '',
  `email` varchar(1024) NOT NULL DEFAULT '',
  `custom_fields` text NOT NULL,
  `transaction_id` varchar(1024) NOT NULL DEFAULT '',
  `total_price` int(11) DEFAULT NULL,
  `generated` int(1) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `#__w7vouchers_orders_states`;

CREATE TABLE `#__w7vouchers_orders_states` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `label` varchar(1024) NOT NULL DEFAULT '',
  `color` varchar(7) NOT NULL DEFAULT '',
  `published` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `#__w7vouchers_orders_states` (`id`, `label`, `color`, `published`)
VALUES
	(1,'COM_W7VOUCHERS_NEW','#457d54',1),
	(2,'COM_W7VOUCHERS_FINISHED','#2a69b8',1),
	(3,'COM_W7VOUCHERS_CANCELED','#c52827',1),
	(4,'COM_W7VOUCHERS_WAITING','#ffb514',1);

DROP TABLE IF EXISTS `#__w7vouchers_payment_methods`;

CREATE TABLE `#__w7vouchers_payment_methods` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(1024) NOT NULL DEFAULT '',
  `service` varchar(1024) DEFAULT NULL,
  `published` int(1) NOT NULL,
  `test_settings` text NOT NULL,
  `production_settings` text NOT NULL,
  `production` int(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `#__w7vouchers_payment_methods` (`id`, `title`, `service`, `published`, `test_settings`, `production_settings`, `production`)
VALUES
	(1,'Credit card','Stripe',1,'{\"api_key\":\"pk_test_51H2DJiLjmaNlMCt8WJZiWq5xwDBUOsiqwTD5qJOZfM8K6q7eysmMGaTWyyaagvcFbhEs6yvmjXnJYkjNqd3objqD00RXHY0JIy\",\"secret_key\":\"sk_test_51H2DJiLjmaNlMCt82vwYs9QIKPU6xm3EMywqalWDyKLyYSaFfp44oQ4vwZO1aC2vmmWVvSxzDEE77m9VWPAxiIss00RJwlWgmH\"}','{}',0);

DROP TABLE IF EXISTS `#__w7vouchers_shipping_methods`;

CREATE TABLE `#__w7vouchers_shipping_methods` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL DEFAULT 'digital',
  `price` int(11) NOT NULL,
  `days` int(11) NOT NULL,
  `description` text NOT NULL,
  `published` int(1) NOT NULL DEFAULT '1',
  `auto_activate` int(1) DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `#__w7vouchers_shipping_methods` (`id`, `title`, `price`, `days`, `description`, `published`, `auto_activate`)
VALUES
	(1,'COM_W7VOUCHERS_ONLINE',0,0,'COM_W7VOUCHERS_ONLINE_DESC',1,1);

DROP TABLE IF EXISTS `#__w7vouchers_stores`;

CREATE TABLE `#__w7vouchers_stores` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(1024) NOT NULL DEFAULT '',
  `description` text NOT NULL,
  `images` text NOT NULL,
  `alias` varchar(1024) NOT NULL DEFAULT '',
  `opening_hours` varchar(1024) NOT NULL DEFAULT '',
  `address` varchar(1024) NOT NULL DEFAULT '',
  `city` varchar(1024) NOT NULL DEFAULT '',
  `postcode` varchar(256) NOT NULL DEFAULT '',
  `published` int(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `alias` (`alias`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `#__w7vouchers_vouchers`;

CREATE TABLE `#__w7vouchers_vouchers` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `id_order` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `id_product` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `expires` datetime NOT NULL,
  `used` int(11) NOT NULL,
  `max_usage` int(11) NOT NULL,
  `code` varchar(12) NOT NULL DEFAULT '',
  `filename` varchar(1024) NOT NULL DEFAULT '',
  `active` int(11) NOT NULL DEFAULT '0',
  `price` int(11) NOT NULL DEFAULT '0',
  `validated` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;