ALTER TABLE #__w7vouchers_orders ADD COLUMN `generated` int(1) DEFAULT '0', AFTER `total_price`;

ALTER TABLE #__w7vouchers_emails ADD COLUMN `params` text, AFTER `type`;

ALTER TABLE #__w7vouchers_emails ADD COLUMN `usertype` varchar(4) DEFAULT 'user', AFTER `params`;

UPDATE `#__w7vouchers_emails` SET `usertype` = 'user';

INSERT INTO `#__w7vouchers_emails` (`id`, `title`, `subject`, `content`, `type`, `params`, `usertype`)
VALUES
	(2,'COM_W7VOUCHERS_ORDER_NOTIFICATION_EMAIL','Máte novou objednávku','<p>Dobrý den,</p>\r\n<p>máte novou objednávku voucherů</p>','admin-notification','{"recipient":"", "send": "1"}','admin');

CREATE UNIQUE INDEX `alias` ON `#__w7vouchers_items` (`alias`);

CREATE UNIQUE INDEX `alias` ON `#__w7vouchers_categories` (`alias`);

CREATE UNIQUE INDEX `alias` ON `#__w7vouchers_stores` (`alias`);