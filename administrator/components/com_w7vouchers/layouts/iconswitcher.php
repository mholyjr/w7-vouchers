<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_w7vouchers
 *
 * @copyright   Copyright (C) 2018 - 2021 W7 Extensions. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die('Restricted access');

$value = $displayData['value'];
$i = $displayData['index'];
$up = $displayData['up'];
$down = $displayData['down'];

?>

<?php if($value) : ?>
    <a class="tbody-icon active" href="javascript:void(0);" onclick="return Joomla.listItemTask('cb<?php echo $i; ?>','<?php echo $down; ?>')" aria-labelledby="cbunpublish<?php echo $i; ?>-desc">
        <span class="icon-publish" aria-hidden="true"></span>
    </a>
<?php else : ?>
    <a class="tbody-icon" href="javascript:void(0);" onclick="return Joomla.listItemTask('cb<?php echo $i; ?>','<?php echo $up; ?>')" aria-labelledby="cbpublish<?php echo $i; ?>-desc">
        <span class="icon-unpublish" aria-hidden="true"></span>
    </a>
<?php endif; ?>