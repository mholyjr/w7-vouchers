<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_w7vouchers
 *
 * @copyright   Copyright (C) 2018 - 2021 W7 Extensions. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die('Restricted access');

/**
 * PaymentMethods table class
 *
 * @since  0.0.1
 */
class W7VouchersTablePaymentMethods extends JTable
{
	/**
	 * Constructor
	 *
	 * @param   JDatabaseDriver  &$db  A database connector object
	 */
	function __construct(&$db)
	{
		parent::__construct('#__w7vouchers_payment_methods', 'id', $db);
	}

	/**
	 * Overloaded bind function
	 *
	 * @param       array           named array
	 * @return      null|string     null is operation was satisfactory, otherwise returns an error
	 * @see JTable:bind
	 * @since 1.5
	 */
	public function bind($array, $ignore = '')
	{
		if (isset($array['test_settings']) && is_array($array['test_settings']))
		{
			$parameter = new JRegistry;
			$parameter->loadArray($array['test_settings']);
			$array['test_settings'] = (string)$parameter;
		}

        if (isset($array['prod_settings']) && is_array($array['prod_settings']))
		{
			$parameter = new JRegistry;
			$parameter->loadArray($array['prod_settings']);
			$array['prod_settings'] = (string)$parameter;
		}

		return parent::bind($array, $ignore);
	}
	
}