<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_w7vouchers
 *
 * @copyright   Copyright (C) 2018 - 2021 W7 Extensions. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die('Restricted access');

/**
 * Items table class
 *
 * @since  0.0.1
 */
class W7VouchersTableItems extends JTable
{
	/**
	 * Constructor
	 *
	 * @param   JDatabaseDriver  &$db  A database connector object
	 */
	function __construct(&$db)
	{
		parent::__construct('#__w7vouchers_items', 'id', $db);
	}

	/**
	 * Overloaded bind function
	 *
	 * @param       array           named array
	 * @return      null|string     null is operation was satisfactory, otherwise returns an error
	 * @see JTable:bind
	 * @since 1.5
	 */
	public function bind($array, $ignore = '')
	{
		if (isset($array['params']) && is_array($array['params']))
		{
			$parameter = new JRegistry;
			$parameter->loadArray($array['params']);
			$array['params'] = (string)$parameter;
		}

		if (isset($array['images']) && is_array($array['images']))
		{
			$parameter = new JRegistry;
			$parameter->loadArray($array['images']);
			$array['images'] = (string)$parameter;
		}

		return parent::bind($array, $ignore);
	}

	public function check()
	{

		$this->alias = trim($this->alias);

		if (empty($this->alias))
		{
			$this->alias = $this->item;
		}

		$this->alias = JFilterOutput::stringURLSafe($this->alias);

		return true;
		
	}

	
}