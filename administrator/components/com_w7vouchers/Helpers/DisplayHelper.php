<?php

namespace W7Extensions\Component\W7Vouchers\Administrator\Helpers;

use Joomla\CMS\Component\ComponentHelper;

/**
 * @package     Joomla.Administrator
 * @subpackage  com_w7vouchers
 *
 * @copyright   Copyright (C) 2018 - 2021 W7 Extensions. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

class DisplayHelper
{

    /**
     * Method to display formatted price
     * 
     * @param   int     $price
     * 
     * @return  string
     */
    public static function displayPrice(int $price): string
    {

        $params                 = ComponentHelper::getParams('com_w7vouchers');
        $currency_symbol        = $params->get('currency_symbol', '€');
        $currency_symbol_pos    = $params->get('currency_symbol_pos', 'left');
        $num_of_decimals        = $params->get('decimals', 2);
        $decimals_sep           = $params->get('decimals_sep', ',');
        $thousands_sep          = $params->get('thousands_sep', ' ');
        $price_format           = '';

        $price = number_format($price, $num_of_decimals, $decimals_sep, $thousands_sep);

        if ($currency_symbol_pos == 'left') {
            $price_format = $currency_symbol . $price;
        } else {
            $price_format = $price . $currency_symbol;
        }

        return $price_format;
    }

    /**
     * Method to display short description
     * 
     * @param   string  $str
     * @param   int     $length
     * 
     * @return  string
     */
    public static function displayShortDesc(string $str, int $length = 120): string
    {
        return mb_strimwidth($str, 0, $length, "...");
    }
}
