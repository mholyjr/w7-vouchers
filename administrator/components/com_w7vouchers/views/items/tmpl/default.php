<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_w7vouchers
 *
 * @copyright   (C) 2008 Open Source Matters, Inc. <https://www.joomla.org>
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

use Joomla\CMS\Router\Route;
use Joomla\CMS\Language\Text;
use Joomla\CMS\HTML\HTMLHelper;
use W7Extensions\Component\W7Vouchers\Site\Helpers\DisplayHelper;
use Joomla\CMS\Layout\LayoutHelper;
use Joomla\CMS\Layout\FileLayout;

$listOrder = $this->escape($this->state->get('list.ordering'));
$listDirn  = $this->escape($this->state->get('list.direction'));
$saveOrder = $listOrder == 'a.ordering';

?>

<form action="<?php echo Route::_('index.php?option=com_w7vouchers&view=items'); ?>" method="post" name="adminForm" id="adminForm">
    <div class="row">
        <div class="col-md-12">
            <div id="j-main-container" class="j-main-container">
                <?php
				// Search tools bar
				echo LayoutHelper::render('joomla.searchtools.default', array('view' => $this));
				?>
                <?php if (empty($this->items)) : ?>
					<div class="alert alert-info">
						<span class="icon-info-circle" aria-hidden="true"></span><span class="visually-hidden"><?php echo Text::_('INFO'); ?></span>
						<?php echo Text::_('JGLOBAL_NO_MATCHING_RESULTS'); ?>
					</div>
				<?php else : ?>
                <table class="table w7_table" id="groupsList">
                    <thead>
					    <tr>
							<td class="w-1 text-center">
								<?php echo HTMLHelper::_('grid.checkall'); ?>
							</td>
                            <th class="w-40" scope="col">
								<?php echo HTMLHelper::_('searchtools.sort', 'COM_W7VOUCHERS_TITLE', 'i.title', $listDirn, $listOrder); ?>
							</th>
                            <th scope="col">
								<?php echo HTMLHelper::_('searchtools.sort', 'JCATEGORY', 'i.cat', $listDirn, $listOrder); ?>
							</th>
                            <th scope="col">
								<?php echo HTMLHelper::_('searchtools.sort', 'COM_W7VOUCHERS_PRICE', 'i.price', $listDirn, $listOrder); ?>
							</th>
                            <th scope="col">
								<?php echo HTMLHelper::_('searchtools.sort', 'JGLOBAL_FIELD_CREATED_LABEL', 'i.created', $listDirn, $listOrder); ?>
							</th>
                            <th scope="col">
								<?php echo HTMLHelper::_('searchtools.sort', 'JPUBLISHED', 'i.published', $listDirn, $listOrder); ?>
							</th>
                            <th scope="col">
								<?php echo HTMLHelper::_('searchtools.sort', 'COM_W7VOUCHERS_ID', 'i.id', $listDirn, $listOrder); ?>
							</th>
                        <tr>
                    </thead>
                    <tfoot>
                    <?php echo $this->pagination->getListFooter(); ?>
                    </tfoot>
                    <tbody>
                    <?php foreach ($this->items as $i => $row) : 
                        $link = Route::_('index.php?option=com_w7vouchers&view=item&layout=edit&id=' . $row->id);
                    ?>
                        <tr>
                            <td>
                                <?php echo HTMLHelper::_('grid.id', $i, $row->id); ?>
                            </td>
                            <td>
                                <a href="<?php echo $link; ?>"><?php echo $this->escape($row->title); ?></a><br>
                                <span class="small">(<?php echo $this->escape($row->alias); ?>)</span>
                                <span class="small"><?php echo DisplayHelper::displayShortDesc($row->description_short, 170); ?></span>
                            </td>
                            <td>
                                <span><?php echo $this->escape($row->cat); ?></span>
                            </td>
                            <td>
                                <span class="<?php echo $row->price == 0 ? 'free' : ''; ?>"><?php echo DisplayHelper::displayPrice($row->price); ?></span>
                            </td>
                            <td>
                                <span><?php echo HtmlHelper::date($row->created, Text::_('DATE_FORMAT_LC5')); ?></span>
                            </td>
                            <td>
                                <?php echo JHtml::_('jgrid.published', $row->published, $i, 'items.', true, 'cb'); ?>
                            </td>
                            <td>
                                <?php echo $row->id; ?>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                    </tbody>
                </table>
                <?php endif; ?>
            </div>
        </div>
    </div>
    <input type="hidden" name="task" value=""/>
	<input type="hidden" name="boxchecked" value="0"/>
	<?php echo JHtml::_('form.token'); ?>
</form>