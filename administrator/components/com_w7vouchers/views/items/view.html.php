<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_w7vouchers
 *
 * @copyright   Copyright (C) 2018 - 2021 W7 Extensions. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access to this file
defined('_JEXEC') or die('Restricted access');

use Joomla\CMS\Toolbar\ToolbarHelper;
use Joomla\CMS\Factory;
use Joomla\CMS\Helper\ContentHelper;

/**
 * Items View
 *
 * @since  0.0.1
 */
class W7VouchersViewItems extends JViewLegacy
{

    /**
	 * Display the Items view
	 *
	 * @param   string  $tpl
	 *
	 * @return  void
	 */
	function display($tpl = null)
	{

        $this->items 		 = $this->get('Items');
		$this->groups 		 = $this->get('Groups');
		$this->pagination    = $this->get('Pagination');
		$this->state         = $this->get('State');
		$this->filterForm    = $this->get('FilterForm');
		$this->activeFilters = $this->get('ActiveFilters');
		$this->canDo 		 = ContentHelper::getActions('com_w7vouchers');

        $this->setDocument();
        $this->addToolBar();

        return parent::display($tpl);
    }

    /**
	 * Add the page title and toolbar.
	 *
	 * @return  void
	 *
	 * @since   1.6
	 */
	protected function addToolBar()
	{
		ToolbarHelper::title(JText::_('COM_W7VOUCHERS_ITEMS_MANAGER'));

		if ($this->canDo->get('core.create')) {
			ToolbarHelper::addNew('item.add');
		}

		if ($this->canDo->get('core.edit')) {
			ToolbarHelper::editList('item.edit');
		}

		if ($this->canDo->get('core.edit.state')) {
			ToolbarHelper::publish('items.publish', 'JTOOLBAR_PUBLISH', true);
			ToolbarHelper::unpublish('items.unpublish', 'JTOOLBAR_UNPUBLISH', true);
		}

		if ($this->state->get('filter.published') == -2 && $this->canDo->get('core.delete')) {
			ToolbarHelper::trash('items.delete');
		} elseif ($this->canDo->get('core.edit.state')) {
			ToolbarHelper::trash('items.trash');
		}

		if ($this->canDo->get('core.admin')) {
			ToolbarHelper::divider();
			ToolbarHelper::preferences('com_w7vouchers');
		}
    }

    /**
	 * Method to set up the document properties
	 *
	 * @return void
	 */
	protected function setDocument() 
	{
		$document = Factory::getDocument();
		$document->setTitle(JText::_('COM_W7VOUCHERS_ITEMS_MANAGER'));
	}
}