<?php

/**
 * @package     Joomla.Administrator
 * @subpackage  com_w7vouchers
 *
 * @copyright   Copyright (C) 2018 - 2021 W7 Extensions. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die('Restricted access');

use Joomla\CMS\HTML\HTMLHelper;
use Joomla\CMS\Language\Associations;
use Joomla\CMS\Language\Text;
use Joomla\CMS\Layout\LayoutHelper;
use Joomla\CMS\Router\Route;
use Joomla\CMS\Uri\Uri;
use W7Extensions\Component\W7Vouchers\Site\Helpers\DisplayHelper;

?>

<div class="row">
    <div class="col-sm-12">
        <div class="voucher_item card">
            <div class="row">
                <div class="col-sm-3">
                    <span class="list-head"><?php echo Text::_('COM_W7VOUCHERS_VOUCHER_NAME'); ?></span>
                </div>
                <div class="col-sm-2">
                    <span class="list-head"><?php echo Text::_('COM_W7VOUCHERS_CODE'); ?></span>
                </div>
                <div class="col-sm-1">
                    <span class="list-head"><?php echo Text::_('COM_W7VOUCHERS_USED'); ?></span>
                </div>
                <div class="col-sm-2">
                    <span class="list-head"><?php echo Text::_('COM_W7VOUCHERS_EXPIRES'); ?></span>
                </div>
                <div class="col-sm-2">
                    <span class="list-head"><?php echo Text::_('COM_W7VOUCHERS_PRICE'); ?></span>
                </div>
                <div class="col-sm-1">
                    <span class="list-head"><?php echo Text::_('COM_W7VOUCHERS_ACTIVE'); ?></span>
                </div>
                <div class="col-sm-1"></div>
            </div>
        </div>
    </div>
</div>
<?php
foreach ($this->vouchers as $voucher) :
    $product_link = JRoute::_('index.php?option=com_w7vouchers&view=item&layout=edit&id=' . $voucher->id_product);
    $download_link = Route::_('index.php?option=com_w7vouchers&task=files.downloadPdf&filename=' . $voucher->filename);
?>
    <div class="row">
        <div class="col-sm-12">
            <div class="voucher_item card">
                <div class="row align-items-center">
                    <div class="col-sm-3">
                        <span>
                            <a href="<?php echo $product_link; ?>"><?php echo $this->escape($voucher->title); ?></a>
                        </span>
                    </div>
                    <div class="col-sm-2">
                        <span class="code"><?php echo $this->escape($voucher->code); ?></span>
                    </div>
                    <div class="col-sm-1">
                        <span class="used"><?php echo $this->escape($voucher->used); ?></span>/<span class="max_use"><?php echo $this->escape($voucher->num_of_use); ?></span>
                    </div>
                    <div class="col-sm-2">
                        <span><?php echo HtmlHelper::date($this->escape($voucher->expires), Text::_('DATE_FORMAT_LC5')); ?></span>
                    </div>
                    <div class="col-sm-2">
                        <span><?php echo DisplayHelper::displayPrice($voucher->price); ?></span>
                    </div>
                    <div class="col-sm-1">
                        <?php if ($voucher->active) : ?>
                            <a class="tbody-icon active" href="<?php echo Route::_('index.php?option=com_w7vouchers&task=order.changeState&id_voucher=' . $voucher->id . '&id=' . $this->item->id . '&state=0'); ?>">
                                <span class="icon-publish" aria-hidden="true"></span>
                            </a>
                        <?php else : ?>
                            <a class="tbody-icon" href="<?php echo Route::_('index.php?option=com_w7vouchers&task=order.changeState&id_voucher=' . $voucher->id . '&id=' . $this->item->id . '&state=1'); ?>">
                                <span class="icon-unpublish" aria-hidden="true"></span>
                            </a>
                        <?php endif; ?>
                    </div>
                    <div class="col-sm-1">
                        <a href="<?php echo $download_link ?>" class="btn btn-primary" target="_blank"><i class="bi bi-download"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php endforeach; ?>