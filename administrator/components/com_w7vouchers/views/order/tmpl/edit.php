<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_w7vouchers
 *
 * @copyright   Copyright (C) 2018 - 2021 W7 Extensions. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die('Restricted access');

use Joomla\CMS\HTML\HTMLHelper;
use Joomla\CMS\Language\Associations;
use Joomla\CMS\Language\Text;
use Joomla\CMS\Layout\LayoutHelper;

$this->configFieldsets  = array('editorConfig');

?>

<form class="w7_item_edit_form w7_form" action="<?php echo JRoute::_('index.php?option=com_w7vouchers&layout=edit&view=order&id=' . (int) $this->item->id); ?>" method="post" name="adminForm" id="adminForm">

	<div class="row mb-4">
		<div class="col-sm-9">
			<div class="card mb-4">
				<?php
					echo HTMLHelper::_('uitab.startTabSet', 'mainTabs', ['active' => 'customer_info', 'recall' => true, 'breakpoint' => 768]);

                    echo HTMLHelper::_('uitab.addTab', 'mainTabs', 'customer_info', Text::_('COM_W7VOUCHERS_CUSTOMER_INFO')); 

                    foreach($this->form->getFieldset('order') as $field) {
                        echo $field->renderField();
                    }

					foreach($this->form->getFieldset('custom') as $field) {
                        echo $field->renderField();
                    }
                
                    echo HTMLHelper::_('uitab.endTab');

					echo HTMLHelper::_('uitab.endTabSet');
				?>
			</div>
			<div id="vouchers_list">
				<?php echo $this->loadTemplate('vouchers'); ?>
			</div>
		</div>
		<div class="col-sm-3">
			<div class="card">
				<?php 
				echo HTMLHelper::_('uitab.startTabSet', 'mainTabs', ['active' => 'settings', 'recall' => true, 'breakpoint' => 768]);

				echo HTMLHelper::_('uitab.addTab', 'mainTabs', 'settings', Text::_('COM_W7VOUCHERS_SETTINGS')); 

				foreach($this->form->getFieldset('additional') as $field) {
					echo $field->renderField();
				}
			
				echo HTMLHelper::_('uitab.endTab');

				echo HTMLHelper::_('uitab.endTabSet');
				?>
			</div>
		</div>
	</div>

    <input type="hidden" name="task" value=""/>
	<input type="hidden" name="boxchecked" value="0"/>
	<?php echo JHtml::_('form.token'); ?>
</form>