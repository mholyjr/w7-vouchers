<?php

/**
 * @package     Joomla.Administrator
 * @subpackage  com_w7vouchers
 *
 * @copyright   Copyright (C) 2018 - 2021 W7 Extensions. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access to this file
defined('_JEXEC') or die('Restricted access');

use Joomla\CMS\Helper\ContentHelper;
use W7Extensions\Component\W7Vouchers\Site\Helpers\FieldsHelper;

/**
 * Order View
 *
 * @since  0.0.1
 */
class W7VouchersViewOrder extends JViewLegacy
{
	/**
	 * View form
	 *
	 * @var         form
	 */
	protected $form = null;

	/**
	 * Display the Item view
	 *
	 * @param   string  $tpl  The name of the template file to parse; automatically searches through the template paths.
	 *
	 * @return  void
	 */
	public function display($tpl = null)
	{
		$this->form 	= $this->get('Form');
		$this->item 	= $this->get('Item');
		$this->canDo 	= ContentHelper::getActions('com_w7vouchers');
		$this->isNew    = ($this->item->id == 0);
		$this->vouchers	= $this->get('Vouchers');
		$fields 		= FieldsHelper::getFormFields();
		$errors 		= empty($this->get('Errors')) ? array() : $this->get('Errors');

		if ($errors) {
			//JError::raiseError(500, implode('<br />', $errors));

			return false;
		}

		foreach ($fields as $field) {
			$this->form->setValue($field->name, 'custom_fields', $this->item->custom_fields[$field->name]);
		}


		$this->addToolBar();
		$this->setDocument();

		parent::display($tpl);
	}

	/**
	 * Add the page title and toolbar.
	 *
	 * @return  void
	 *
	 * @since   1.6
	 */
	protected function addToolBar()
	{
		$input = JFactory::getApplication()->input;

		$input->set('hidemainmenu', true);

		if ($this->isNew) {
			$title = JText::_('COM_W7VOUCHERS_MANAGER_ITEM_NEW');
		} else {
			$title = JText::_('COM_W7VOUCHERS_MANAGER_ITEM_EDIT');
		}

		JToolbarHelper::title($title, 'item');

		if ($this->canDo->get('core.edit')) {
			JToolbarHelper::apply('order.apply');
			JToolbarHelper::save('order.save');
		}

		JToolbarHelper::cancel(
			'order.cancel',
			$isNew ? 'JTOOLBAR_CANCEL' : 'JTOOLBAR_CLOSE'
		);

		if ($this->canDo->get('core.edit')) {
			JToolbarHelper::custom('order.generate', 'plus', 'plus', 'Generovat vouchery', false);
		}
	}

	/**
	 * Method to set up the document properties
	 *
	 * @return void
	 */
	protected function setDocument()
	{
		$document = JFactory::getDocument();

		if ($this->isNew) {
			$title = JText::_('COM_W7VOUCHERS_MANAGER_ITEM_NEW');
		} else {
			$title = JText::_('COM_W7VOUCHERS_MANAGER_ITEM_EDIT');
		}

		$document->setTitle($title);
	}
}
