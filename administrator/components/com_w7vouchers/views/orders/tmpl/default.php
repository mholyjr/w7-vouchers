<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_w7vouchers
 *
 * @copyright   (C) 2008 Open Source Matters, Inc. <https://www.joomla.org>
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

use Joomla\CMS\Router\Route;
use Joomla\CMS\Language\Text;
use Joomla\CMS\HTML\HTMLHelper;
use W7Extensions\Component\W7Vouchers\Site\Helpers\DisplayHelper;
use Joomla\CMS\Layout\LayoutHelper;
use Joomla\CMS\Layout\FileLayout;

$listOrder = $this->escape($this->state->get('list.ordering'));
$listDirn  = $this->escape($this->state->get('list.direction'));
$saveOrder = $listOrder == 'a.ordering';

?>

<form action="<?php echo Route::_('index.php?option=com_w7vouchers&view=orders'); ?>" method="post" name="adminForm" id="adminForm">
    <div class="row">
        <div class="col-md-12">
            <div id="j-main-container" class="j-main-container">
                <?php
				// Search tools bar
				echo LayoutHelper::render('joomla.searchtools.default', array('view' => $this));
				?>
                <?php if (empty($this->items)) : ?>
					<div class="alert alert-info">
						<span class="icon-info-circle" aria-hidden="true"></span><span class="visually-hidden"><?php echo Text::_('INFO'); ?></span>
						<?php echo Text::_('JGLOBAL_NO_MATCHING_RESULTS'); ?>
					</div>
				<?php else : ?>
                <table class="table w7_table" id="groupsList">
                    <thead>
					    <tr>
							<td class="w-1 text-center">
								<?php echo HTMLHelper::_('grid.checkall'); ?>
							</td>
                            <th scope="col">
								<?php echo HTMLHelper::_('searchtools.sort', 'COM_W7VOUCHERS_FIRST_NAME', 'i.first_name', $listDirn, $listOrder); ?>
							</th>
                            <th scope="col">
								<?php echo HTMLHelper::_('searchtools.sort', 'COM_W7VOUCHERS_LAST_NAME', 'i.last_name', $listDirn, $listOrder); ?>
							</th>
                            <th scope="col">
								<?php echo HTMLHelper::_('searchtools.sort', 'COM_W7VOUCHERS_EMAIL', 'i.email', $listDirn, $listOrder); ?>
							</th>
                            <th scope="col">
								<?php echo HTMLHelper::_('searchtools.sort', 'COM_W7VOUCHERS_TOTAL_PRICE', 'i.total_price', $listDirn, $listOrder); ?>
							</th>
                            <th scope="col">
								<?php echo HTMLHelper::_('searchtools.sort', 'COM_W7VOUCHERS_CREATED', 'i.created', $listDirn, $listOrder); ?>
							</th>
                            <th scope="col">
								<?php echo HTMLHelper::_('searchtools.sort', 'COM_W7VOUCHERS_STATE', 'i.state', $listDirn, $listOrder); ?>
							</th>
                            <th scope="col">
								<?php echo HTMLHelper::_('searchtools.sort', 'COM_W7VOUCHERS_ID', 'i.id', $listDirn, $listOrder); ?>
							</th>
                        <tr>
                    </thead>
                    <tbody>
                    <?php foreach ($this->items as $i => $row) : 
                        $link = JRoute::_('index.php?option=com_w7vouchers&view=order&layout=edit&id=' . $row->id, false);
                    ?>
                        <tr>
                            <td>
                                <?php echo HTMLHelper::_('grid.id', $i, $row->id); ?>
                            </td>
                            <td>
                                <a href="<?php echo $link; ?>"><?php echo $row->first_name; ?></a>
                            </td>
                            <td>
                                <a href="<?php echo $link; ?>"><?php echo $row->last_name; ?></a>
                            </td>
                            <td>
                                <a href="<?php echo $link; ?>"><?php echo $row->email; ?></a>
                            </td>
                            <td>
                                <span><?php echo DisplayHelper::displayPrice((int)$row->total_price); ?></span>
                            </td>
                            <td>
                                <span><?php echo HtmlHelper::date($row->created, Text::_('DATE_FORMAT_LC5')); ?></span>
                            </td>
                            <td>
                                <span class="pill" style="background-color: <?php echo $this->escape($row->tag_color); ?>">
                                    <?php echo Text::_($row->tag_label); ?>
                                </span>
                            </td>
                            <td>
                                <?php echo $row->id; ?>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                    </tbody>
                    <tfoot>
                    <?php echo $this->pagination->getListFooter(); ?>
                    </tfoot>
                </table>
                <?php endif; ?>
            </div>
        </div>
    </div>
    <input type="hidden" name="task" value=""/>
	<input type="hidden" name="boxchecked" value="0"/>
	<?php echo JHtml::_('form.token'); ?>
</form>