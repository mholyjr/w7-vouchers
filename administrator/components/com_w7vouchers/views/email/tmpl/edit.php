<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_w7vouchers
 *
 * @copyright   Copyright (C) 2018 - 2021 W7 Extensions. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die('Restricted access');

use Joomla\CMS\HTML\HTMLHelper;
use Joomla\CMS\Language\Associations;
use Joomla\CMS\Language\Text;
use Joomla\CMS\Layout\LayoutHelper;

$this->configFieldsets  = array('editorConfig');

?>

<form class="w7_item_edit_form w7_form" action="<?php echo JRoute::_('index.php?option=com_w7vouchers&layout=edit&id=' . (int) $this->item->id); ?>" method="post" name="adminForm" id="adminForm">

    <div class="row">
        <div class="col-sm-9">
        <?php echo LayoutHelper::render('joomla.edit.title_alias', $this); ?>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <div class="card">
                <?php 
                    echo HTMLHelper::_('uitab.startTabSet', 'mainTabs', ['active' => 'settigns', 'recall' => true, 'breakpoint' => 768]);

                    echo HTMLHelper::_('uitab.addTab', 'mainTabs', 'settigns', Text::_('COM_W7VOUCHERS_SETTINGS')); 

                    foreach($this->form->getFieldset('settings') as $field) {
                        echo $field->renderField();
                    }

                    if($this->item->usertype == 'admin')
                    {
                        foreach($this->form->getFieldset('paramsFields') as $field) {
                            echo $field->renderField();
                        }
                    }
                    
                
                    echo HTMLHelper::_('uitab.endTab');

                    echo HTMLHelper::_('uitab.endTabSet');
                    ?>
            </div>
        </div>
    </div>
    <input type="hidden" name="task" value="category.edit" />
    <?php echo JHtml::_('form.token'); ?>
</form>