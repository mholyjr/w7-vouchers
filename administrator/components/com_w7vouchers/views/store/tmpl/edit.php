<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_w7vouchers
 *
 * @copyright   Copyright (C) 2018 - 2021 W7 Extensions. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die('Restricted access');

use Joomla\CMS\HTML\HTMLHelper;
use Joomla\CMS\Language\Associations;
use Joomla\CMS\Language\Text;
use Joomla\CMS\Layout\LayoutHelper;

$this->configFieldsets  = array('editorConfig');

?>

<form class="w7_item_edit_form w7_form" action="<?php echo JRoute::_('index.php?option=com_w7vouchers&layout=edit&id=' . (int) $this->item->id); ?>" method="post" name="adminForm" id="adminForm">
    <div class="row">
        <div class="col-sm-9">
        <?php echo LayoutHelper::render('joomla.edit.title_alias', $this); ?>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-9">
            <div class="card">
                <?php 
                    echo HTMLHelper::_('uitab.startTabSet', 'mainTabs', ['active' => 'location', 'recall' => true, 'breakpoint' => 768]);

                    echo HTMLHelper::_('uitab.addTab', 'mainTabs', 'location', Text::_('COM_W7VOUCHERS_LOCATION')); 

                    foreach($this->form->getFieldset('location') as $field) {
                        echo $field->renderField();
                    }
                
                    echo HTMLHelper::_('uitab.endTab');

                    echo HTMLHelper::_('uitab.addTab', 'mainTabs', 'hours', Text::_('COM_W7VOUCHERS_OPENING_HOURS')); 

                    foreach($this->form->getFieldset('hours') as $field) {
                        echo $field->renderField();
                    }
                
                    echo HTMLHelper::_('uitab.endTab');

                    echo HTMLHelper::_('uitab.addTab', 'mainTabs', 'images', Text::_('COM_W7VOUCHERS_IMAGES')); 

                    foreach($this->form->getFieldset('imagesSet') as $field) {
                        echo $field->renderField();
                    }
                
                    echo HTMLHelper::_('uitab.endTab');

                    echo HTMLHelper::_('uitab.endTabSet');
                    ?>
            </div>
        </div>
        <div class="col-sm-3">
            <div class="card">
                <?php
                echo HTMLHelper::_('uitab.startTabSet', 'secondaryTabs', ['active' => 'publishing', 'recall' => true, 'breakpoint' => 768]);
                echo HTMLHelper::_('uitab.addTab', 'secondaryTabs', 'publishing', Text::_('COM_W7VOUCHERS_PUBLISHING')); 
                
                foreach($this->form->getFieldset('publishing') as $field) {
                    echo $field->renderField();
                }
    
                echo HTMLHelper::_('uitab.endTab');
                echo HTMLHelper::_('uitab.endTabSet');
                ?>
            </div>
        </div>
    </div>
    <input type="hidden" name="task" value="category.edit" />
    <?php echo JHtml::_('form.token'); ?>
</form>