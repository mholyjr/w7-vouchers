<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_w7vouchers
 *
 * @copyright   (C) 2008 Open Source Matters, Inc. <https://www.joomla.org>
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

use Joomla\CMS\Factory;
use Joomla\CMS\Router\Route;
use Joomla\CMS\Language\Text;
use Joomla\CMS\HTML\HTMLHelper;
use W7Extensions\Component\W7Vouchers\Administrator\Helpers\DisplayHelper;
use Joomla\CMS\Layout\LayoutHelper;
use Joomla\CMS\Layout\FileLayout;

$user = Factory::getUser();
$userId = $user->get('id');

$listOrder = $this->escape($this->state->get('list.ordering'));
$listDirn  = $this->escape($this->state->get('list.direction'));
$saveOrder = $listOrder == 'a.ordering';

$switch = new FileLayout('iconswitcher', JPATH_ROOT .'/components/com_w7vouchers/layouts');

?>

<form action="<?php echo Route::_('index.php?option=com_w7vouchers&view=categories'); ?>" method="post" name="adminForm" id="adminForm">
    <div class="row">
        <div class="col-md-12">
            <div id="j-main-container" class="j-main-container">
                <?php
				// Search tools bar
				//echo LayoutHelper::render('joomla.searchtools.default', array('view' => $this));
				?>
                <?php if (empty($this->items)) : ?>
					<div class="alert alert-info">
						<span class="icon-info-circle" aria-hidden="true"></span><span class="visually-hidden"><?php echo Text::_('INFO'); ?></span>
						<?php echo Text::_('JGLOBAL_NO_MATCHING_RESULTS'); ?>
					</div>
				<?php else : ?>
                <table class="table" id="groupsList">
                    <thead>
					    <tr>
                            <th class="w-1 text-center">
                                <?php echo HTMLHelper::_('searchtools.sort', '', 'ordering', $listDirn, $listOrder, null, 'asc', 'JGRID_HEADING_ORDERING', 'icon-menu-2'); ?>
                            </th>
							<th class="w-1 text-center">
								<?php echo HTMLHelper::_('grid.checkall'); ?>
							</th>
                            <th scope="col">
								<?php echo HTMLHelper::_('searchtools.sort', 'COM_W7VOUCHERS_LABEL', 'i.label', $listDirn, $listOrder); ?>
                            </th>
                            <th scope="col">
								<?php echo HTMLHelper::_('searchtools.sort', 'COM_W7VOUCHERS_TYPE', 'i.type', $listDirn, $listOrder); ?>
							</th>
                            <th scope="col">
								<?php echo HTMLHelper::_('searchtools.sort', 'COM_W7VOUCHERS_NAME', 'i.name', $listDirn, $listOrder); ?>
							</th>
                            <th scope="col">
								<?php echo HTMLHelper::_('searchtools.sort', 'COM_W7VOUCHERS_CORE', 'i.core', $listDirn, $listOrder); ?>
							</th>
                            <th scope="col">
								<?php echo HTMLHelper::_('searchtools.sort', 'COM_W7VOUCHERS_REQUIRED', 'i.required', $listDirn, $listOrder); ?>
							</th>
                            <th scope="col">
								<?php echo HTMLHelper::_('searchtools.sort', 'JPUBLISHED', 'i.published', $listDirn, $listOrder); ?>
							</th>
                            <th scope="col">
								<?php echo HTMLHelper::_('searchtools.sort', 'COM_W7VOUCHERS_ID', 'i.id', $listDirn, $listOrder); ?>
							</th>
                        <tr>
                    </thead>
                    <tbody>
                    <?php foreach ($this->items as $i => $row) : 
                        $link = JRoute::_('index.php?option=com_w7vouchers&view=field&layout=edit&id=' . $row->id);
                        $data_required = ['value' => $row->required, 'index' => $i, 'up' => 'field.required', 'down' => 'field.notRequired'];
                        $switch_required = $switch->render($data_required);
                    ?>
                        <tr>
                            <td>
                                <?php
                                $iconClass = '';
                                $canReorder  = $user->authorise('core.edit.state', 'com_w7vouchers.field.' . $row->id);
                                if (!$canReorder)
                                {
                                    $iconClass = ' inactive';
                                }
                                elseif (!$saveOrder)
                                {
                                    $iconClass = ' inactive tip-top hasTooltip" title="' . JHtml::_('tooltipText', 'JORDERINGDISABLED');
                                }
                                ?>
                                <span class="sortable-handler<?php echo $iconClass ?>">
                                    <span class="icon-menu" aria-hidden="true"></span>
                                </span>
                                <?php if ($canReorder && $saveOrder) : ?>
                                    <input type="text" style="display:none" name="order[]" size="5" value="<?php echo $row->ordering; ?>" class="width-20 text-area-order" />
                                <?php endif; ?>
                            </td>
                            <td>
                                <?php echo HTMLHelper::_('grid.id', $i, $row->id); ?>
                            </td>
                            <td>
                                <a href="<?php echo $link; ?>"><?php echo Text::_($row->label); ?></a><br>
                            </td>
                            <td>
                                <?php echo $row->type; ?>
                            </td>
                            <td>
                                <?php echo $row->name; ?>
                            </td>
                            <td>
                                <a class="tbody-icon <?php echo $row->core == 1 ? 'active' : ''; ?>" href="javascript:void(0);">
                                    <span class="<?php echo $row->core == 1 ? 'icon-publish' : 'icon-unpublish'; ?>" aria-hidden="true"></span>
                                </a>
                            </td>
                            <td>
                                <?php echo $switch_required; ?>
                            </td>
                            <td>
                                <?php if($row->core == 1) : ?>
                                <a class="tbody-icon active" href="javascript:void(0);">
                                    <span class="icon-publish" aria-hidden="true"></span>
                                </a>
                                <?php else : ?>
                                <?php echo JHtml::_('jgrid.published', $row->published, $i, 'fields.', true, 'cb'); ?>
                                <?php endif; ?>
                            </td>
                            <td>
                                <?php echo $row->id; ?>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                    </tbody>
                </table>
                <?php endif; ?>
            </div>
        </div>
    </div>
    <input type="hidden" name="task" value=""/>
	<input type="hidden" name="boxchecked" value="0"/>
	<?php echo JHtml::_('form.token'); ?>
</form>