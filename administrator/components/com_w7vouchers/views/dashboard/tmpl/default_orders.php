<?php

/**
 * @package     Joomla.Administrator
 * @subpackage  com_w7vouchers
 *
 * @copyright   (C) 2008 Open Source Matters, Inc. <https://www.joomla.org>
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

use Joomla\CMS\Router\Route;
use Joomla\CMS\Language\Text;
use Joomla\CMS\HTML\HTMLHelper;
use W7Extensions\Component\W7Vouchers\Site\Helpers\DisplayHelper;
use Joomla\CMS\Layout\LayoutHelper;
use Joomla\CMS\Layout\FileLayout;

$colors = array('#9638D7', '#E24B91', '#38B0C2', '#7A7876', '#30DB84');

?>

<div id="orders_list" class="card">
    <div class="card-header d-flex justify-space-between">
        <span><?php echo Text::_('COM_W7VOUCHERS_LAST_ORDERS'); ?></span>
        <a href="<?php echo Route::_('index.php?option=com_w7vouchers&view=orders', false); ?>" class="btn btn-outline-primary btn-sm"><?php echo Text::_('COM_W7VOUCHERS_VIEW_ALL'); ?></a>
    </div>
    <?php if (count($this->orders)) : foreach ($this->orders as $order) :
            $link = Route::_('index.php?option=com_w7vouchers&view=order&layout=edit&id=' . $order->id, false);
    ?>
            <div class="row m-0 order-row">
                <div class="w-10">
                    <span class="customer_name_icon" style="background-color: <?php echo $colors[rand(0, 4)]; ?>;">
                        <?php echo $order->first_name[0];
                        echo $order->last_name[0]; ?>
                    </span>
                </div>
                <div class="w-70">
                    <span class="customer_name">
                        <?php echo $this->escape($order->first_name); ?> <?php echo $this->escape($order->last_name); ?> (<?php echo $this->escape($order->email); ?>)
                    </span>
                    <span class="order_created">
                        <?php echo HtmlHelper::date($order->created, Text::_('DATE_FORMAT_LC5')); ?> |
                        <?php echo DisplayHelper::displayPrice($order->total_price); ?> |
                        <?php if ($order->paid) : ?>
                            <span class="success"><?php echo Text::_('COM_W7VOUCHERS_PAID'); ?></span>
                        <?php else : ?>
                            <span class="danger"><?php echo Text::_('COM_W7VOUCHERS_UNPAID'); ?></span>
                        <?php endif; ?>
                    </span>
                </div>
                <div class="w-20">
                    <a href="<?php echo $link; ?>" class="btn btn-fluid btn-outline-primary"><?php echo Text::_('COM_W7VOUCHERS_VIEW'); ?></a>
                </div>
            </div>
        <?php endforeach;
    else : ?>
        <div class="p-3">
            <div class="alert alert-info">
                <span class="icon-info-circle" aria-hidden="true"></span><span class="visually-hidden"><?php echo Text::_('INFO'); ?></span>
                <?php echo Text::_('JGLOBAL_NO_MATCHING_RESULTS'); ?>
            </div>
        </div>

    <?php endif; ?>

</div>