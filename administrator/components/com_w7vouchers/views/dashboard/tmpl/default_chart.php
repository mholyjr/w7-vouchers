<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_w7vouchers
 *
 * @copyright   (C) 2008 Open Source Matters, Inc. <https://www.joomla.org>
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

use Joomla\CMS\Router\Route;
use Joomla\CMS\Language\Text;
use Joomla\CMS\HTML\HTMLHelper;

?>

<div id="orders_chart_wrapper" class="card">
    <?php 
    echo HTMLHelper::_('uitab.startTabSet', 'mainTabs', ['active' => 'settigns', 'recall' => true, 'breakpoint' => 768]);

    echo HTMLHelper::_('uitab.addTab', 'mainTabs', 'ordersChart', Text::_('COM_W7VOUCHERS_ORDERS_OVERVIEW')); 
    ?>
    <canvas id="orders_chart"></canvas>
    <?php
    echo HTMLHelper::_('uitab.endTab');

    echo HTMLHelper::_('uitab.addTab', 'mainTabs', 'totalChart', Text::_('COM_W7VOUCHERS_TOTAL_PRICE_OVERVIEW')); 
    ?>
    <canvas id="total_chart"></canvas>
    <?php
    echo HTMLHelper::_('uitab.endTab');

    echo HTMLHelper::_('uitab.endTabSet');
    ?>
</div>

<script>
const orders = document.getElementById('orders_chart');
const total = document.getElementById('total_chart');

new Chart(orders, {
    type: 'line',
    data: {
        labels: [<?php echo $this->ordersChart['y']; ?>],
        datasets: [{
            label: '<?php echo Text::_('COM_W7VOUCHERS_NUMBER_OF_ORDERS'); ?>',
            data: [<?php echo $this->ordersChart['x']; ?>],
            borderWidth: 3,
            borderColor: '#132f53',
            tension: 0.3,
            fill: true,
            backgroundColor: '#132f5345'
        }]
    },
    options: {
        scales: {
            y: {
                beginAtZero: true
            }
        },
        aspectRatio: 1|3
    }
});

new Chart(total, {
    type: 'line',
    data: {
        labels: [<?php echo $this->totalChart['y']; ?>],
        datasets: [{
            label: '<?php echo Text::_('COM_W7VOUCHERS_TOTAL_PRICE'); ?>',
            data: [<?php echo $this->totalChart['x']; ?>],
            borderWidth: 3,
            borderColor: '#132f53',
            tension: 0.3,
            fill: true,
            backgroundColor: '#132f5345'
        }]
    },
    options: {
        scales: {
            y: {
                beginAtZero: true
            }
        },
        aspectRatio: 1|3
    }
});
</script>