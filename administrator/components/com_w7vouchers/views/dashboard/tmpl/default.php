<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_w7vouchers
 *
 * @copyright   (C) 2008 Open Source Matters, Inc. <https://www.joomla.org>
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

use Joomla\CMS\Router\Route;
use Joomla\CMS\Language\Text;
use Joomla\CMS\HTML\HTMLHelper;
use W7Extensions\Component\W7Vouchers\Site\Helpers\DisplayHelper;
use Joomla\CMS\Layout\LayoutHelper;
use Joomla\CMS\Layout\FileLayout;

?>

<form action="<?php echo Route::_('index.php?option=com_w7vouchers&view=dashboard'); ?>" method="post" name="adminForm" id="adminForm">

<div class="row mb-4">
    <div class="col-sm-3">
        <div class="info_card">
            <div class="w-70">
                <span class="info_card_label small">
                    <?php echo Text::_('COM_W7VOUCHERS_TODAYS_ORDERS'); ?>
                </span>
                <span class="info_card_value">
                    <?php echo $this->todayOrders; ?>
                </span>
            </div>
            <div class="w-30 info_card_icon">
                <i class="bi bi-bag-check"></i>
            </div>
        </div>
    </div>
    <div class="col-sm-3">
        <div class="info_card">
            <div class="w-70">
                <span class="info_card_label small">
                    <?php echo Text::_('COM_W7VOUCHERS_TODAYS_TOTAL'); ?>
                </span>
                <span class="info_card_value">
                    <?php echo DisplayHelper::displayPrice($this->todayTotal); ?>
                </span>
            </div>
            <div class="w-30 info_card_icon">
                <i class="bi bi-cash-stack"></i>
            </div>
        </div>
    </div>
    <div class="col-sm-3">
        <div class="info_card">
            <div class="w-70">
                <span class="info_card_label small">
                    <?php echo Text::_('COM_W7VOUCHERS_NUMBER_OF_VOUCHERS'); ?>
                </span>
                <span class="info_card_value">
                    <?php echo $this->todayVouchers; ?>
                </span>
            </div>
            <div class="w-30 info_card_icon">
                <i class="bi bi-qr-code"></i>
            </div>
        </div>
    </div>
    <div class="col-sm-3">
        <div class="info_card">
            <div class="w-70">
                <span class="info_card_label small">
                    <?php echo Text::_('COM_W7VOUCHERS_TODAYS_REDEEMS'); ?>
                </span>
                <span class="info_card_value">
                    <?php echo $this->todayRedeems; ?>
                </span>
            </div>
            <div class="w-30 info_card_icon">
                <i class="bi bi-qr-code-scan"></i>
            </div>
        </div>
    </div>
</div>

<div class="row mb-4">
    <div class="col-sm-12">
        <?php echo $this->loadTemplate('chart'); ?>
    </div>
</div>

<div class="row">
    <div class="col-sm-6">
        <?php echo $this->loadTemplate('orders'); ?>
    </div>
</div>

</form>