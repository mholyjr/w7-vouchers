<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_w7vouchers
 *
 * @copyright   Copyright (C) 2018 - 2021 W7 Extensions. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die('Restricted access');

use Joomla\CMS\Factory;
use Joomla\CMS\Helper\ContentHelper;
use Joomla\CMS\Toolbar\ToolbarHelper;

/**
 * Dashboard View
 *
 * @since  0.0.1
 */
class W7VouchersViewDashboard extends JViewLegacy
{
	/**
	 * Display the Dashboard view
	 *
	 * @param   string  $tpl  The name of the template file to parse; automatically searches through the template paths.
	 *
	 * @return  void
	 */
	public function display($tpl = null)
	{
        $app = Factory::getApplication();
		$this->orders = $this->get('Orders');

		$this->ordersChart = $this->get('OrdersChart');
		$this->totalChart = $this->get('TotalChart');
		$this->todayOrders = $this->get('TodayOrders');
		$this->todayTotal = $this->get('TodayTotal');
		$this->todayVouchers = $this->get('TodayVouchers');
		$this->todayRedeems = $this->get('TodayRedeems');

		$this->setDocument();

		parent::display($tpl);
	}

	/**
	 * Method to set up the document properties
	 *
	 * @return void
	 */
	protected function setDocument() 
	{
		$document = Factory::getDocument();
		$document->setTitle(JText::_('COM_W7VOUCHERS_DASHBOARD_MANAGER'));
		$document->addScript('components/com_w7vouchers/assets/js/chart.js');
	}
}