<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_w7vouchers
 *
 * @copyright   (C) 2008 Open Source Matters, Inc. <https://www.joomla.org>
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

use Joomla\CMS\Router\Route;
use Joomla\CMS\Language\Text;
use Joomla\CMS\HTML\HTMLHelper;
use Joomla\CMS\Layout\LayoutHelper;

$listOrder = $this->escape($this->state->get('list.ordering'));
$listDirn  = $this->escape($this->state->get('list.direction'));
$saveOrder = $listOrder == 'a.ordering';

?>
<form action="<?php echo Route::_('index.php?option=com_w7vouchers&view=customers'); ?>" method="post" name="adminForm" id="adminForm">
    <div class="row">
        <div class="col-md-12">
            <div id="j-main-container" class="j-main-container">
                <?php
				// Search tools bar
				//echo LayoutHelper::render('joomla.searchtools.default', array('view' => $this));
				?>
                <?php if (empty($this->items)) : ?>
					<div class="alert alert-info">
						<span class="icon-info-circle" aria-hidden="true"></span><span class="visually-hidden"><?php echo Text::_('INFO'); ?></span>
						<?php echo Text::_('JGLOBAL_NO_MATCHING_RESULTS'); ?>
					</div>
				<?php else : ?>
                <table class="table" id="groupsList">
                    <thead>
					    <tr>
							<td class="w-1 text-center">
								<?php echo HTMLHelper::_('grid.checkall'); ?>
							</td>
                            <th scope="col">
								<?php echo HTMLHelper::_('searchtools.sort', 'COM_W7VOUCHERS_NAME', 'u.name', $listDirn, $listOrder); ?>
                            </th>
                            <th scope="col">
								<?php echo HTMLHelper::_('searchtools.sort', 'COM_W7VOUCHERS_EMAIL', 'u.email', $listDirn, $listOrder); ?>
							</th>
                            <th scope="col">
								<?php echo HTMLHelper::_('searchtools.sort', 'COM_W7VOUCHERS_REGISTERED_DATE', 'u.registerDate', $listDirn, $listOrder); ?>
							</th>
                            <th scope="col">
								<?php echo HTMLHelper::_('searchtools.sort', 'COM_W7VOUCHERS_LAST_LOGIN', 'u.registerDate', $listDirn, $listOrder); ?>
							</th>
                            <th scope="col">
								<?php echo HTMLHelper::_('searchtools.sort', 'COM_W7VOUCHERS_ID', 'u.id', $listDirn, $listOrder); ?>
							</th>
                        <tr>
                    </thead>
                    <tbody>
                    <?php foreach ($this->items as $i => $row) : 
                        $link = JRoute::_('index.php?option=com_w7vouchers&view=customer&layout=edit&id=' . $row->id);
                    ?>
                        <tr>
                            <td>
                                <?php echo HTMLHelper::_('grid.id', $i, $row->id); ?>
                            </td>
                            <td>
                                <?php echo $this->escape($row->name); ?>
                            </td>
                            <td>
                                <?php echo $this->escape($row->email); ?>
                            </td>
                            <td>
                                <span><?php echo HtmlHelper::date($row->registerDate, Text::_('DATE_FORMAT_LC5')); ?></span>
                            </td>
                            <td>
                                <span><?php echo HtmlHelper::date($row->lastvisitDate, Text::_('DATE_FORMAT_LC5')); ?></span>
                            </td>
                            <td>
                                <?php echo $row->id; ?>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                    </tbody>
                </table>
                <?php endif; ?>
            </div>
        </div>
    </div>
    <input type="hidden" name="task" value=""/>
	<input type="hidden" name="boxchecked" value="0"/>
	<?php echo JHtml::_('form.token'); ?>
</form>