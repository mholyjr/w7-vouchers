<?php

/**
 * @package     Joomla.Administrator
 * @subpackage  com_w7vouchers
 *
 * @copyright   Copyright (C) 2018 - 2021 W7 Extensions. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die('Restricted access');

use Joomla\CMS\Factory;
use Joomla\Database\ParameterType;

/**
 * Items Model
 *
 * @since  0.0.1
 */
class W7VouchersModelItems extends JModelList
{

	/**
	 * Constructor.
	 *
	 * @param   array  $config  An optional associative array of configuration settings.
	 *
	 * @see     JController
	 * @since   1.6
	 */
	public function __construct($config = array())
	{

		if (empty($config['filter_fields'])) {
			$config['filter_fields'] = array(
				'id', 'i.id',
				'id_cateogry', 'i.id_cateogry',
				'published', 'i.published'
			);
		}

		parent::__construct($config);
	}

	/**
	 * Method to auto-populate the model state.
	 *
	 * Note. Calling getState in this method will result in recursion.
	 *
	 * @param   string  $ordering   An optional ordering field.
	 * @param   string  $direction  An optional direction (asc|desc).
	 *
	 * @return  void
	 *
	 * @since   1.6
	 * @throws  \Exception
	 */
	protected function populateState($ordering = 'i.title', $direction = 'asc')
	{
		$app = Factory::getApplication();

		// List state information.
		parent::populateState($ordering, $direction);
	}

	/**
	 * Method to build an SQL query to load the list data.
	 *
	 * @return      string  An SQL query
	 */
	protected function getListQuery()
	{
		$db    = Factory::getDbo();
		$query = $db->getQuery(true);

		$query->select('i.*, c.title as cat')
			->from($db->quoteName('#__w7vouchers_items', 'i'))
			->join('LEFT', $db->quoteName('#__w7vouchers_categories', 'c') . ' ON ' . $db->quoteName('i.catid') . ' = ' . $db->quoteName('c.id'));

		$filter_published = $this->getState('filter.published');
		$filter_category = $this->getState('filter.id_category');
		$search = $this->getState('filter.search');

		if (is_numeric($filter_published)) {
			$query->where($db->quoteName('i.published') . ' = :published')
				->bind(':published', $filter_published, ParameterType::INTEGER);
		} else {
			$query->where($db->quoteName('i.published') . ' IN (0,1)');
		}

		if (is_numeric($filter_category)) {
			$query->where($db->quoteName('i.catid') . ' = :id_category')
				->bind(':id_category', $filter_category, ParameterType::INTEGER);
		}

		if (!empty($search)) {
			if (mb_stripos($search, 'id:') === 0) {
				$ids = (int) substr($search, 3);
				$query->where($db->quoteName('i.id') . ' = :id');
				$query->bind(':id', $ids, ParameterType::INTEGER);
			} elseif (mb_stripos($search, 'title:') === 0) {
				$search = '%' . mb_substr($search, 6) . '%';
				$query->where($db->quoteName('i.title') . ' LIKE :title');
				$query->bind(':title', $search);
			} else {
				$search = '%' . trim($search) . '%';
				$query->where($db->quoteName('i.title') . ' LIKE :title')
					->bind(':title', $search);
			}
		}

		$query->order(
			$db->quoteName($db->escape($this->getState('list.ordering', 'i.title'))) . ' ' . $db->escape($this->getState('list.direction', 'ASC'))
		);

		return $query;
	}
}
