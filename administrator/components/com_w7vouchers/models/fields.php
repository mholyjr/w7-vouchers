<?php

/**
 * @package     Joomla.Administrator
 * @subpackage  com_w7vouchers
 *
 * @copyright   Copyright (C) 2018 - 2021 W7 Extensions. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die('Restricted access');

use Joomla\CMS\Factory;

/**
 * Fields Model
 *
 * @since  0.0.1
 */
class W7VouchersModelFields extends JModelList
{

	/**
	 * Constructor.
	 *
	 * @param   array  $config  An optional associative array of configuration settings.
	 *
	 * @see     JController
	 * @since   1.6
	 */
	public function __construct($config = array())
	{

		if (empty($config['filter_fields'])) {
			$config['filter_fields'] = array(
				'id', 'i.id',
				'label', 'i.label',
				'core', 'i.core',
				'required', 'i.required',
				'published', 'i.published'
			);
		}

		parent::__construct($config);
	}

	/**
	 * Method to auto-populate the model state.
	 *
	 * Note. Calling getState in this method will result in recursion.
	 *
	 * @param   string  $ordering   An optional ordering field.
	 * @param   string  $direction  An optional direction (asc|desc).
	 *
	 * @return  void
	 *
	 * @since   1.6
	 * @throws  \Exception
	 */
	protected function populateState($ordering = 'i.id', $direction = 'desc')
	{
		$app = Factory::getApplication();

		// List state information.
		parent::populateState($ordering, $direction);
	}

	/**
	 * Method to build an SQL query to load the list data.
	 *
	 * @return      string  An SQL query
	 */
	protected function getListQuery()
	{
		$db    = Factory::getDbo();
		$query = $db->getQuery(true);

		$query->select('i.*')
			->from($db->quoteName('#__w7vouchers_fields', 'i'));

		$search = $this->getState('filter.search');

		if (!empty($search)) {
			$search = '%' . trim($search) . '%';

			$query->where($db->quoteName('i.label') . ' LIKE :label')
				->bind(':label', $search);
		}

		$query->order(
			$db->quoteName($db->escape($this->getState('list.ordering', 'i.id'))) . ' ' . $db->escape($this->getState('list.direction', 'DESC'))
		);

		return $query;
	}
}
