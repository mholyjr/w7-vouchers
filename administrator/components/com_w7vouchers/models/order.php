<?php

/**
 * @package     Joomla.Administrator
 * @subpackage  com_w7vouchers
 *
 * @copyright   Copyright (C) 2018 - 2021 W7 Extensions. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die('Restricted access');

use Joomla\CMS\Factory;
use Joomla\Registry\Registry;
use W7Extensions\Component\W7Vouchers\Site\Helpers\DisplayHelper;
use W7Extensions\Component\W7Vouchers\Site\Helpers\FieldsHelper;

/**
 * Item Model
 *
 * @since  0.0.1
 */
class W7VouchersModelOrder extends JModelAdmin
{
	/**
	 * Method to override getItem to allow us to convert the JSON-encoded image information
	 * in the database record into an array for subsequent prefilling of the edit form
	 */
	public function getItem($pk = null)
	{
		$item = parent::getItem($pk);

		if ($item and property_exists($item, 'custom_fields')) {
			$registry = new Registry($item->custom_fields);
			$item->custom_fields = $registry->toArray();
		}

		return $item;
	}

	/**
	 * Method to get a table object, load it if necessary.
	 *
	 * @param   string  $type    The table name. Optional.
	 * @param   string  $prefix  The class prefix. Optional.
	 * @param   array   $config  Configuration array for model. Optional.
	 *
	 * @return  JTable  A JTable object
	 *
	 * @since   1.6
	 */
	public function getTable($type = 'Orders', $prefix = 'W7VouchersTable', $config = array())
	{
		return JTable::getInstance($type, $prefix, $config);
	}

	/**
	 * Method to get the record form.
	 *
	 * @param   array    $data      Data for the form.
	 * @param   boolean  $loadData  True if the form is to load its own data (default case), false if not.
	 *
	 * @return  mixed    A JForm object on success, false on failure
	 *
	 * @since   1.6
	 */
	public function getForm($data = array(), $loadData = true)
	{

		$fields = FieldsHelper::getFormFields();

		$form = $this->loadForm(
			'com_w7vouchers.order',
			'order',
			array(
				'control' => 'jform',
				'load_data' => $loadData
			)
		);

		foreach ($fields as $field) {
			if ($field->type != 'password') {
				if ($field->type != 'select') {
					$field_xml = new SimpleXMLElement('
                        <field
                            name="' . $field->name . '"
                            type="' . $field->type . '"
                            label="' . $field->label . '"
                            required="' . DisplayHelper::trueFalse($field->required) . '"
                        />
                    ');

					$form->setField($field_xml, null, false, 'custom');
				}
			}
		}

		return $form;
	}

	/**
	 * Method to get the data that should be injected in the form.
	 *
	 * @return  mixed  The data for the form.
	 *
	 * @since   1.6
	 */
	protected function loadFormData()
	{
		$data = JFactory::getApplication()->getUserState(
			'com_w7vouchers.edit.order.data',
			array()
		);

		if (empty($data)) {
			$data = $this->getItem();
		}

		return $data;
	}

	/**
	 * Method to change the published state of one or more records.
	 *
	 * @param   array    &$pks   A list of the primary keys to change.
	 * @param   integer  $value  The value of the published state.
	 *
	 * @return  boolean  True on success.
	 *
	 * @since   1.6
	 */
	public function publish(&$pks, $value = 1)
	{
		$dispatcher = Factory::getApplication()->getDispatcher();
		$user = Factory::getUser();
		$table = $this->getTable();
		$pks = (array) $pks;

		// Include the plugins for the change of state event.
		\JPluginHelper::importPlugin($this->events_map['change_state']);

		// Access checks.
		foreach ($pks as $i => $pk) {
			$table->reset();

			if ($table->load($pk)) {
				if (!$this->canEditState($table)) {
					// Prune items that you can't change.
					unset($pks[$i]);

					\JLog::add(\JText::_('JLIB_APPLICATION_ERROR_EDITSTATE_NOT_PERMITTED'), \JLog::WARNING, 'jerror');

					return false;
				}

				// If the table is checked out by another user, drop it and report to the user trying to change its state.
				if (property_exists($table, 'checked_out') && $table->checked_out && ($table->checked_out != $user->id)) {
					\JLog::add(\JText::_('JLIB_APPLICATION_ERROR_CHECKIN_USER_MISMATCH'), \JLog::WARNING, 'jerror');

					// Prune items that you can't change.
					unset($pks[$i]);

					return false;
				}

				/**
				 * Prune items that are already at the given state.  Note: Only models whose table correctly
				 * sets 'published' column alias (if different than published) will benefit from this
				 */
				$publishedColumnName = $table->getColumnAlias('published');

				if (property_exists($table, $publishedColumnName) && $table->get($publishedColumnName, $value) == $value) {
					unset($pks[$i]);

					continue;
				}
			}
		}

		// Check if there are items to change
		if (!count($pks)) {
			return true;
		}

		// Attempt to change the state of the records.
		if (!$table->publish($pks, $value, $user->get('id'))) {
			$this->setError($table->getError());

			return false;
		}

		$context = $this->option . '.' . $this->name;

		// Clear the component's cache
		$this->cleanCache();

		return true;
	}

	/**
	 * Method to override the JModelAdmin save() function to handle Save as Copy correctly
	 *
	 * @param   array The voucher record data submitted from the form.
	 *
	 * @return  parent::save() return value
	 */
	public function save($data)
	{
		return parent::save($data);
	}

	/**
	 * Method to get list of vouchers
	 * 
	 * @return  array
	 */
	public function getVouchers($pk = null): array
	{
		$db = Factory::getDbo();
		$app = Factory::getApplication();
		$input = $app->input;
		$id = $input->get('id', 'INT', 0);

		$query = $db->getQuery(true);

		$query->select('v.*, p.title, p.num_of_use')
			->from($db->quoteName('#__w7vouchers_vouchers', 'v'))
			->join('LEFT', $db->quoteName('#__w7vouchers_items', 'p') . ' ON ' . $db->quoteName('v.id_product') . ' = ' . $db->quoteName('p.id'))
			->where($db->quoteName('v.id_order') . ' = :id_order')
			->bind(':id_order', $id);

		$db->setQuery($query);
		$items = $db->loadObjectList();

		return $items;
	}

	/**
	 * Method to change state of the voucher
	 * 
	 * @param	int		$id_voucher
	 * @param	int		$state
	 * 
	 * @return	bool
	 */
	public function changeState(int $id_voucher, int $state): bool
	{
		$db = Factory::getDbo();
		$voucher = new stdClass();
		$voucher->id = $id_voucher;
		$voucher->active = $state;
		$db->updateObject('#__w7vouchers_vouchers', $voucher, 'id', false);
		return true;
	}
}
