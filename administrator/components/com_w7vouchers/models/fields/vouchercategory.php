<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_helloworld
 *
 * @copyright   Copyright (C) 2005 - 2018 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access to this file
defined('_JEXEC') or die('Restricted access');

JFormHelper::loadFieldClass('list');

/**
 * HelloWorld Form Field class for the HelloWorld component
 *
 * @since  0.0.1
 */
class JFormFieldVoucherCategory extends JFormFieldList
{
	/**
	 * The field type.
	 *
	 * @var         string
	 */
	protected $type = 'VoucherCategory';

	/**
	 * Method to get a list of options for a list input.
	 *
	 * @return  array  An array of JHtml options.
	 */
	protected function getOptions()
	{
		$cat_ext = $this->getAttribute('extension');
		$db    = JFactory::getDBO();
		$query = $db->getQuery(true);

        $query->select('id, title')
            ->from($db->quoteName('#__w7vouchers_categories'))
            ->where($db->quoteName('published') . ' = ' . 1);

		$db->setQuery((string) $query);
		$categories = $db->loadObjectList();
		$options  = array();

		if ($categories)
		{
			foreach ($categories as $category)
			{
				$options[] = JHtml::_('select.option', $category->id, $category->title);
			}
		}

		$options = array_merge(parent::getOptions(), $options);

		return $options;
	}
}