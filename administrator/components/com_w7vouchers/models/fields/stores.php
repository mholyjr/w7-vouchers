<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_helloworld
 *
 * @copyright   Copyright (C) 2005 - 2018 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die('Restricted access');

use Joomla\CMS\Language\Text;

JFormHelper::loadFieldClass('list');

/**
 * HelloWorld Form Field class for the HelloWorld component
 *
 * @since  0.0.1
 */
class JFormFieldStores extends JFormFieldList
{
	/**
	 * The field type.
	 *
	 * @var         string
	 */
	protected $type = 'Stores';

	/**
	 * Method to get a list of options for a list input.
	 *
	 * @return  array  An array of JHtml options.
	 */
	protected function getOptions()
	{
		$cat_ext = $this->getAttribute('extension');
		$db    = JFactory::getDBO();
		$query = $db->getQuery(true);

        $query->select('id, title')
            ->from($db->quoteName('#__w7vouchers_stores'))
            ->where($db->quoteName('published') . ' = ' . 1);

		$db->setQuery((string) $query);
		$states = $db->loadObjectList();
		$options  = array();

		if ($states)
		{
			foreach ($states as $state)
			{
				$options[] = JHtml::_('select.option', $state->id, $state->title);
			}
		}

		$options = array_merge(parent::getOptions(), $options);

		return $options;
	}
}