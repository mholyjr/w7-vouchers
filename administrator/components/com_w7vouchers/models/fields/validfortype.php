<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_helloworld
 *
 * @copyright   Copyright (C) 2005 - 2018 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access to this file
defined('_JEXEC') or die('Restricted access');

JFormHelper::loadFieldClass('list');

/**
 * HelloWorld Form Field class for the HelloWorld component
 *
 * @since  0.0.1
 */
class JFormFieldValidForType extends JFormFieldList
{
	/**
	 * The field type.
	 *
	 * @var         string
	 */
	protected $type = 'ValidForType';

	/**
	 * Method to get a list of options for a list input.
	 *
	 * @return  array  An array of JHtml options.
	 */
	protected function getOptions()
	{

		$options[] = JHtml::_('select.option', 'hours', JText::_('COM_W7VOUCHERS_VALUE_HOURS'));
        $options[] = JHtml::_('select.option', 'days', JText::_('COM_W7VOUCHERS_VALUE_DAYS'));
        $options[] = JHtml::_('select.option', 'weeks', JText::_('COM_W7VOUCHERS_VALUE_WEEKS'));
        $options[] = JHtml::_('select.option', 'months', JText::_('COM_W7VOUCHERS_VALUE_MONTHS'));
        $options[] = JHtml::_('select.option', 'years', JText::_('COM_W7VOUCHERS_VALUE_YEARS'));

		$options = array_merge(parent::getOptions(), $options);

		return $options;
	}
}