<?php

/**
 * @package     Joomla.Administrator
 * @subpackage  com_w7vouchers
 *
 * @copyright   Copyright (C) 2018 - 2021 W7 Extensions. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die('Restricted access');

use Joomla\CMS\Factory;
use Joomla\CMS\MVC\Model\AdminModel;

/**
 * Dashboard Model
 *
 * @since  0.0.1
 */
class W7VouchersModelDashboard extends AdminModel
{

    /**
     * Method to get last 10 orders
     * 
     * @return  array
     */
    public function getOrders(): array
    {
        $db = Factory::getDbo();
        $query = $db->getQuery(true);

        $query->select('i.*, s.label as tag_label, s.color as tag_color')
            ->from($db->quoteName('#__w7vouchers_orders', 'i'))
            ->join('LEFT', $db->quoteName('#__w7vouchers_orders_states', 's') . ' ON ' . $db->quoteName('i.state') . ' = ' . $db->quoteName('s.id'))
            ->setLimit('5')
            ->order($db->quoteName('i.created') . ' DESC');

        $db->setQuery($query);
        $data = $db->loadObjectList();

        return $data;
    }

    /**
     * Method to get orders data for the chart
     * 
     * @return  array
     */
    public function getOrdersChart(): array
    {
        $db = Factory::getDbo();
        $query = $db->getQuery(true);

        $query->select('COUNT(*) as count, substring(`created`,1,10) as date')
            ->from($db->quoteName('#__w7vouchers_orders'))
            ->group('substring(created,1,10)');

        $db->setQuery($query);
        $data = $db->loadObjectList();

        $x = '';
        $y = '';
        foreach ($data as $item) {
            $x .= $item->count . ',';
            $y .= '"' . $item->date . '"' . ',';
        }

        $axis = array('x' => $x, 'y' => $y);

        return $axis;
    }

    /**
     * Method to get orders data for the chart
     * 
     * @return  array
     */
    public function getTotalChart(): array
    {
        $db = Factory::getDbo();
        $query = $db->getQuery(true);

        $query->select('SUM(total_price) as count, substring(`created`,1,10) as date')
            ->from($db->quoteName('#__w7vouchers_orders'))
            ->group('substring(created,1,10)');

        $db->setQuery($query);
        $data = $db->loadObjectList();

        $x = '';
        $y = '';
        foreach ($data as $item) {
            $x .= $item->count . ',';
            $y .= '"' . $item->date . '"' . ',';
        }

        $axis = array('x' => $x, 'y' => $y);

        return $axis;
    }

    /**
     * Method to get today's orders
     * 
     * @return  int
     */
    public function getTodayOrders(): int
    {
        $db = Factory::getDbo();
        $query = $db->getQuery(true);

        $query->select('COUNT(*) as count')
            ->from($db->quoteName('#__w7vouchers_orders'))
            ->where('DATE(' . $db->quoteName('created') . ') = CURDATE()');

        $db->setQuery($query);
        $count = $db->loadResult();

        return (int)$count;
    }

    /**
     * Method to get number vouchers sold today
     * 
     * @return  int
     */
    public function getTodayVouchers(): int
    {
        $db = Factory::getDbo();
        $query = $db->getQuery(true);

        $query->select('COUNT(*) as count')
            ->from($db->quoteName('#__w7vouchers_vouchers'))
            ->where('DATE(' . $db->quoteName('created') . ') = CURDATE()');

        $db->setQuery($query);
        $count = $db->loadResult();

        return (int)$count;
    }

    /**
     * Method to get number of today's redeems
     * 
     * @return  int
     */
    public function getTodayRedeems(): int
    {
        $db = Factory::getDbo();
        $query = $db->getQuery(true);

        $query->select('COUNT(*) as count')
            ->from($db->quoteName('#__w7vouchers_vouchers'))
            ->where('DATE(' . $db->quoteName('validated') . ') = CURDATE()');

        $db->setQuery($query);
        $count = $db->loadResult();

        return (int)$count;
    }

    /**
     * Method to get today's total price
     * 
     * @return  int
     */
    public function getTodayTotal(): int
    {
        $db = Factory::getDbo();
        $query = $db->getQuery(true);

        $query->select('SUM(total_price) as total_price')
            ->from($db->quoteName('#__w7vouchers_orders'))
            ->where('DATE(' . $db->quoteName('created') . ') = CURDATE()');

        $db->setQuery($query);
        $total_price = $db->loadResult();

        return (int)$total_price;
    }


    public function getForm($data = [], $loadData = true)
    {
    }
}
