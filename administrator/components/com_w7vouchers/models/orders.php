<?php

/**
 * @package     Joomla.Administrator
 * @subpackage  com_w7vouchers
 *
 * @copyright   Copyright (C) 2018 - 2021 W7 Extensions. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die('Restricted access');

use Joomla\CMS\Factory;
use Joomla\Database\ParameterType;

/**
 * Orders Model
 *
 * @since  0.0.1
 */
class W7VouchersModelOrders extends JModelList
{

	/**
	 * Constructor.
	 *
	 * @param   array  $config  An optional associative array of configuration settings.
	 *
	 * @see     JController
	 * @since   1.6
	 */
	public function __construct($config = array())
	{

		if (empty($config['filter_fields'])) {
			$config['filter_fields'] = array(
				'id', 'i.id',
				'state', 'i.state',
				'created', 'i.created',
				'total_price', 'i.total_price',
				'email', 'i.email',
				'first_name', 'i.first_name',
				'last_name', 'i.last_name',
				'published', 'i.published'
			);
		}

		parent::__construct($config);
	}

	/**
	 * Method to auto-populate the model state.
	 *
	 * Note. Calling getState in this method will result in recursion.
	 *
	 * @param   string  $ordering   An optional ordering field.
	 * @param   string  $direction  An optional direction (asc|desc).
	 *
	 * @return  void
	 *
	 * @since   1.6
	 * @throws  \Exception
	 */
	protected function populateState($ordering = 'i.id', $direction = 'desc')
	{
		$app = Factory::getApplication();

		// List state information.
		parent::populateState($ordering, $direction);
	}

	/**
	 * Method to build an SQL query to load the list data.
	 *
	 * @return      string  An SQL query
	 */
	protected function getListQuery()
	{
		$db    = Factory::getDbo();
		$query = $db->getQuery(true);

		$query->select('i.*, s.label as tag_label, s.color as tag_color')
			->from($db->quoteName('#__w7vouchers_orders', 'i'))
			->join('LEFT', $db->quoteName('#__w7vouchers_orders_states', 's') . ' ON ' . $db->quoteName('i.state') . ' = ' . $db->quoteName('s.id'));

		$search = $this->getState('filter.search');

		$filter_state = $this->getState('filter.state');
		$filter_date_from = $this->getState('filter.date_from');
		$filter_date_to = $this->getState('filter.date_to');
		$filter_payment = $this->getState('filter.paid');

		if (!empty($filter_date_from)) {
			$query->where($db->quoteName('i.created') . ' >= ' . $db->quote($filter_date_from));
		}

		if (!empty($filter_date_to)) {
			$query->where($db->quoteName('i.created') . ' <= ' . $db->quote($filter_date_to));
		}

		if (is_numeric($filter_state)) {
			$query->where($db->quoteName('i.state') . ' = :state')
				->bind(':state', $filter_state, ParameterType::INTEGER);
		}

		if (is_numeric($filter_payment)) {
			$query->where($db->quoteName('i.paid') . ' = :payment')
				->bind(':payment', $filter_payment, ParameterType::INTEGER);
		}

		if (!empty($search)) {
			if (mb_stripos($search, 'id:') === 0) {
				$ids = (int) substr($search, 3);
				$query->where($db->quoteName('i.id') . ' = :id');
				$query->bind(':id', $ids, ParameterType::INTEGER);
			} elseif (mb_stripos($search, 'firstname:') === 0) {
				$search = '%' . mb_substr($search, 10) . '%';
				$query->where($db->quoteName('i.first_name') . ' LIKE :firstname');
				$query->bind(':firstname', $search);
			} elseif (mb_stripos($search, 'lastname:') === 0) {
				$search = '%' . mb_substr($search, 9) . '%';
				$query->where($db->quoteName('i.last_name') . ' LIKE :lastname');
				$query->bind(':lastname', $search);
			} elseif (mb_stripos($search, 'email:') === 0) {
				$search = '%' . mb_substr($search, 6) . '%';
				$query->where($db->quoteName('i.email') . ' LIKE :email');
				$query->bind(':email', $search);
			} else {
				$search = '%' . trim($search) . '%';
				$query
					->where($db->quoteName('i.first_name') . ' LIKE :firstname')
					->orWhere($db->quoteName('i.last_name') . ' LIKE :lastname')
					->orWhere($db->quoteName('i.email') . ' LIKE :email')
					->bind(':firstname', $search)
					->bind(':lastname', $search)
					->bind(':email', $search);
			}
		}

		$query->order(
			$db->quoteName($db->escape($this->getState('list.ordering', 'i.id'))) . ' ' . $db->escape($this->getState('list.direction', 'DESC'))
		);

		return $query;
	}
}
