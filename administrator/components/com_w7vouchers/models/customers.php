<?php

/**
 * @package     Joomla.Administrator
 * @subpackage  com_w7vouchers
 *
 * @copyright   Copyright (C) 2018 - 2021 W7 Extensions. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die('Restricted access');

use Joomla\CMS\Factory;
use Joomla\CMS\Component\ComponentHelper;

/**
 * Customers Model
 *
 * @since  0.0.1
 */
class W7VouchersModelCustomers extends JModelList
{
	/**
	 * Constructor.
	 *
	 * @param   array  $config  An optional associative array of configuration settings.
	 *
	 * @see     JController
	 * @since   1.6
	 */
	public function __construct($config = array())
	{
		if (empty($config['filter_fields'])) {
			$config['filter_fields'] = array(
				'id', 'i.id',
				'title', 'i.title',
				'enabled', 'i.enabled',
				'readonly', 'i.readonly',
				'published', 'i.published'
			);
		}

		$params = ComponentHelper::getParams('com_w7vouchers');
		$this->params = $params;


		parent::__construct($config);
	}

	/**
	 * Method to auto-populate the model state.
	 *
	 * Note. Calling getState in this method will result in recursion.
	 *
	 * @param   string  $ordering   An optional ordering field.
	 * @param   string  $direction  An optional direction (asc|desc).
	 *
	 * @return  void
	 *
	 * @since   1.6
	 * @throws  \Exception
	 */
	protected function populateState($ordering = 'u.id', $direction = 'desc')
	{
		// List state information.
		parent::populateState($ordering, $direction);
	}

	/**
	 * Method to build an SQL query to load the list data.
	 *
	 * @return      string  An SQL query
	 */
	protected function getListQuery()
	{
		$customer_group = $this->params->get('customer_usergroup', 2);
		$db    = Factory::getDbo();
		$query = $db->getQuery(true);

		$query->select('u.*')
			->from($db->quoteName('#__user_usergroup_map', 'ug'))
			->join('LEFT', $db->quoteName('#__users', 'u') . ' ON ' . $db->quoteName('ug.user_id') . ' = ' . $db->quoteName('u.id'))
			->where($db->quoteName('ug.group_id') . ' = ' . $customer_group);

		$query->order(
			$db->quoteName($db->escape($this->getState('list.ordering', 'u.id'))) . ' ' . $db->escape($this->getState('list.direction', 'DESC'))
		);

		return $query;
	}
}
