<?php

/**
 * @package     Joomla.Administrator
 * @subpackage  com_w7vouchers
 *
 * @copyright   Copyright (C) 2018 - 2021 W7 Extensions. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access to this file
defined('_JEXEC') or die('Restricted access');

use Joomla\CMS\Factory;
use Joomla\CMS\Response\JsonResponse;
use Joomla\Utilities\ArrayHelper;
use Joomla\CMS\Input\Input;
use W7Extensions\Component\W7Vouchers\Site\Helpers\FilesHelper;
use Joomla\CMS\Language\Text;


/**
 * Order Controller
 *
 * @package     Joomla.Administrator
 * @subpackage  com_w7cookies
 * @since       0.0.9
 */
class W7VouchersControllerOrder extends JControllerForm
{

	/**
	 * Proxy for getModel.
	 *
	 * @param string $name The model name. Optional.
	 * @param string $prefix The class prefix. Optional.
	 * @param array $config Configuration array for model. Optional.
	 *
	 * @return JModelLegacy The model.
	 *
	 * @since  1.6
	 */
	public function getModel($name = 'Order', $prefix = 'W7VouchersModel', $config = [])
	{
		// Get the model object
		$model = parent::getModel($name, $prefix, $config);

		return $model;
	}


	/**
	 * Method to generate vouchers
	 */
	public function generate()
	{
		$app = JFactory::getApplication();
		$input = $app->input;
		$id_order = $input->getInt('id');

		try {
			FilesHelper::createVoucher($id_order);
			$app->enqueueMessage(JText::_('COM_W7VOUCHERS_VOUCHERS_GENERATED'), 'success');
		} catch (Exception $e) {
			throw $e;
			$app->enqueueMessage(JText::_('COM_W7VOUCHERS_ERROR_CREATING_VOUCHERS'), 'error');
		}

		$url = JRoute::_('index.php?option=' . $this->option . '&layout=edit&view=order&id=' . $id_order, false);
		$this->setRedirect($url);
	}


	public function changeState()
	{
		$app = JFactory::getApplication();
		$input = $app->input;
		$id_voucher = $input->getInt('id_voucher');

		// Publish/unpublish
		$state = $input->getInt('state');
		$id_order = $input->getInt('id');

		$model = $this->getModel();

		if ($model->changeState($id_voucher, $state)) {
			$message = $state ? JText::_('COM_W7VOUCHERS_VOUCHER_PUBLISHED') : JText::_('COM_W7VOUCHERS_VOUCHER_UNPUBLISHED');
			$app->enqueueMessage($message, 'success');
		} else {
			$app->enqueueMessage(JText::_('COM_W7VOUCHERS_ERROR_CHANGING_STATE'), 'error');
		}

		$url = JRoute::_('index.php?option=' . $this->option . '&layout=edit&view=order&id=' . $id_order, false);
		$this->setRedirect($url);
	}
}
