<?php

/**
 * @package     Joomla.Administrator
 * @subpackage  com_w7vouchers
 *
 * @copyright   Copyright (C) 2018 - 2021 W7 Extensions. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die('Restricted access');

use Joomla\CMS\Factory;
use Joomla\CMS\Filesystem\File;

/**
 * Files Controller
 *
 * @package     Joomla.Administrator
 * @subpackage  com_w7vouchers
 * @since       0.0.9
 */
class W7VouchersControllerFiles extends JControllerForm
{

    /**
     * Method to download protected pdf file
     * 
     * @return  mixed   false|void
     */
    public function downloadPdf()
    {
        $app = Factory::getApplication();
        $user = Factory::getUser();

        if ($user->id == 0) {
            $app->enqueueMessage(Text::_('COM_W7VOUCHERS_NOT_AUTHORIZED'), 'error');
            return false;
        }

        $input = $app->input;
        $filename = $input->get('filename', 'STRING', '');
        $id_order = $this->getFolder($filename);

        $file = JPATH_SITE . '/media/com_w7vouchers/pdf/' . $id_order . '/' . $filename . '.pdf';

        if (File::exists($file)) {
            header("Content-type: application/pdf");
            header('Content-Disposition: attachment; filename="' . basename($filename) . '.pdf"');
            readfile($file);
            exit;
        }
    }

    /**
     * Method to get order's folder from filename
     * 
     * @param   string  $filename
     * 
     * @return  int
     */
    private function getFolder(string $filename): int
    {
        $db = Factory::getDbo();
        $query = $db->getQuery(true);

        $query->select('id_order')
            ->from($db->quoteName('#__w7vouchers_vouchers'))
            ->where($db->quoteName('filename') . ' = :filename')
            ->bind(':filename', $filename);

        $db->setQuery($query);
        $id = $db->loadResult();

        return (int)$id;
    }
}
